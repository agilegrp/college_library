package GUI;

import java.awt.Color;
import java.awt.EventQueue;
import java.sql.*;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JRadioButton;

import model.User;
import controller.PlaceReq;

public class GUIPlaceReq extends PlaceReq {
	static User user;//remove static later when
	private JFrame frame;
	private JTextField textFtitle;
	private JTextField textFisbn;
	private JTextField textFstart;
	private JTextField textFend;
	
	String tetFtitle;
	String tetFisbn;
		static int  tetFstart;
	static int tetFend;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUIPlaceReq window = new GUIPlaceReq(user);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUIPlaceReq(final User user) {
	/*	initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	//private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setBackground(Color.CYAN);
		
	
		
		textFtitle = new JTextField();
		textFtitle.setText("");
		textFtitle.setBounds(125, 111, 137, 23);
		frame.getContentPane().add(textFtitle);
		textFtitle.setColumns(10);
		
		textFisbn = new JTextField();
		textFisbn.setText("");
		textFisbn.setColumns(10);
		textFisbn.setBounds(125, 145, 137, 23);
		frame.getContentPane().add(textFisbn);
		
		JLabel lblTitle = new JLabel("Title");
		lblTitle.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblTitle.setBounds(31, 114, 86, 14);
		frame.getContentPane().add(lblTitle);
		
		JLabel lblIsbn = new JLabel("ISBN");
		lblIsbn.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblIsbn.setBounds(31, 152, 86, 14);
		frame.getContentPane().add(lblIsbn);
		
		JButton button = new JButton("Back to Menu");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
		
			}
		});
		button.setBounds(79, 327, 117, 23);
		frame.getContentPane().add(button);
		
		JLabel lblStartPage = new JLabel("Start Page");
		lblStartPage.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblStartPage.setBounds(31, 210, 86, 23);
		frame.getContentPane().add(lblStartPage);
		
		JLabel lblEndPage = new JLabel("End Page");
		lblEndPage.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblEndPage.setBounds(31, 244, 86, 20);
		frame.getContentPane().add(lblEndPage);
		
		textFstart = new JTextField();
		textFstart.setText("0");
		textFstart.setColumns(10);
		textFstart.setBounds(125, 211, 137, 23);
		frame.getContentPane().add(textFstart);
		
		textFend = new JTextField();
		textFend.setText("0");
		textFend.setColumns(10);
		textFend.setBounds(125, 241, 137, 23);
		frame.getContentPane().add(textFend);
		
		
		JRadioButton rdbtnBook = new JRadioButton("Book");
		JRadioButton rdbtnJournals = new JRadioButton("Journals");
	
		rdbtnBook.setBounds(79, 41, 67, 23);
		rdbtnJournals.setBounds(153, 41, 109, 23);


        ButtonGroup group = new ButtonGroup();

		group.add(rdbtnBook);		
		group.add(rdbtnJournals);
		
		frame.getContentPane().add(rdbtnBook);
		frame.getContentPane().add(rdbtnJournals);
		
		JLabel lblPlaceRequest = new JLabel("Place Request ");
		lblPlaceRequest.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblPlaceRequest.setBounds(98, 11, 204, 23);
		frame.getContentPane().add(lblPlaceRequest);
		
		JLabel lblVisableOnlyFor = new JLabel("-- Next fields only Visable only for Journals");
		lblVisableOnlyFor.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		lblVisableOnlyFor.setBounds(31, 185, 231, 14);
		frame.getContentPane().add(lblVisableOnlyFor);
		
		JLabel lblInputTitleOr = new JLabel("-- Input Title OR ISBN");
		lblInputTitleOr.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		lblInputTitleOr.setBounds(79, 86, 133, 14);
		frame.getContentPane().add(lblInputTitleOr);
		rdbtnJournals.setSelected(true);

		rdbtnJournals.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 textFstart.setEnabled(true);
				 textFend.setEnabled(true);
			}
		});
		
		rdbtnBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//set TF to zero before its unvisable;
				textFstart.setText("0");
				textFend.setText("0");
				textFstart.setEnabled(false);
				textFend.setEnabled(false);
				 
				 
			}
		});
		
		
		JButton btnPlaceRequest = new JButton("Place Request");
		btnPlaceRequest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PlaceReq pr = new PlaceReq();
				int userid = user.getId();
				String ref = "";
	 		//	String ref1 = "";
				if(textFtitle.getText().equals("") && textFisbn.getText().equals(""))
		 		{
		 			JOptionPane.showMessageDialog(null,
					        "Invalid data entered/ Must Input title OR isbn ", 
							"Place Request", 
							JOptionPane.PLAIN_MESSAGE);
		 		}else {
				tetFtitle = textFtitle.getText();
				tetFisbn = textFisbn.getText();
				tetFstart=  Integer.parseInt(textFstart.getText());
				tetFend=  Integer.parseInt(textFend.getText());
				try {
						ref=	pr.gettitle(tetFtitle, userid,tetFstart,tetFend);
				
				} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				if(textFtitle.getText().equals("")){
					ref=	pr.getisbn(tetFisbn, userid,tetFstart,tetFend);
				}
			JOptionPane.showMessageDialog(null,"REF :  "+ref,"Note!",
		 		        JOptionPane.INFORMATION_MESSAGE);
			}
			}
		});
		btnPlaceRequest.setBounds(26, 285, 123, 23);
		frame.getContentPane().add(btnPlaceRequest);
		
		JButton btnCancelRequest = new JButton("Cancel Request");
		btnCancelRequest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				GUICancelReq cr = new GUICancelReq();
				cr.frame.setVisible(true);
			}
		});
		btnCancelRequest.setBounds(153, 285, 123, 23);
		frame.getContentPane().add(btnCancelRequest);
		
		frame.setSize(313, 400);
		frame.setVisible(true);
	}
}

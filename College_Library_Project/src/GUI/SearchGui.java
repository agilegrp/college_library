package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;

import javax.swing.JTextField;

import controller.SearchItem;
import model.Item;
import model.User;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class SearchGui implements ActionListener {

	Item item;
	User user;
	private JFrame frame;
	private JTextField textFieldB_Title;
	private JTextField textFieldB_Author;
	private JTextField textFieldISBN;
	private JTextField textFieldB_LibRefNo;
	private JTextField textFieldJ_Title;
	private JTextField textFieldJ_Author;
	private JTextField textFieldJ_libRefNo;
	private JTextField textFieldBookStkNum;
	private JTextField textFieldJStkNum;
	
	private JButton searchBTitleButton;
	private JButton searchJTitleButton;
	private JButton searchBAuthorButton;
	private JButton searchJAuthorButton;
	private JButton searchBRefButton;
	private JButton searchJRefButton;
	private JButton searchISBNButton;
	private JButton clearButton;
	private JButton placeRequestButton;
	private JButton mainMenuButton;
	

	/**
	 *  Launch
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SearchGui window = new SearchGui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SearchGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.CYAN);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);
		
		frame.setTitle("Project Group B3");
		frame.setBounds(100, 100, 817, 305);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
//-------------------------------------------Labels----------------------------------------------------------//				
		JLabel lblSearchBooks = new JLabel("Search Books");
		lblSearchBooks.setFont(new Font("Serif", Font.BOLD, 20));
		lblSearchBooks.setBounds(76, 2, 124, 23);
		frame.getContentPane().add(lblSearchBooks);
		
		JLabel lblTitle = new JLabel("Title");
		lblTitle.setBounds(10, 36, 66, 14);
		frame.getContentPane().add(lblTitle);
		
		JLabel lblAuthor = new JLabel("Author");
		lblAuthor.setBounds(10, 61, 66, 14);
		frame.getContentPane().add(lblAuthor);
						
		JLabel lblLibrefno = new JLabel("LibRefNo");
		lblLibrefno.setBounds(10, 111, 66, 14);
		frame.getContentPane().add(lblLibrefno);
		
		JLabel lblIsbn = new JLabel("ISBN");
		lblIsbn.setBounds(10, 86, 66, 14);
		frame.getContentPane().add(lblIsbn);
		
		JLabel lblSearchJournals = new JLabel("Search Journals");
		lblSearchJournals.setFont(new Font("Serif", Font.BOLD, 20));
		lblSearchJournals.setBounds(400, 2, 168, 23);
		frame.getContentPane().add(lblSearchJournals);
		
		JLabel lblTitle_J = new JLabel("Title");
		lblTitle_J.setBounds(380, 36, 80, 14);
		frame.getContentPane().add(lblTitle_J);
		
		JLabel lblAuthor_J = new JLabel("Author");
		lblAuthor_J.setBounds(380, 61, 80, 14);
		frame.getContentPane().add(lblAuthor_J);
		
		JLabel lblLibrefno_J = new JLabel("LibRefNo");
		lblLibrefno_J.setBounds(380, 86, 80, 14);
		frame.getContentPane().add(lblLibrefno_J);
		
		JLabel lblStockno = new JLabel("StockNo");
		lblStockno.setBounds(10, 136, 66, 14);
		frame.getContentPane().add(lblStockno);
		
		JLabel lblStockno_1 = new JLabel("StockNo");
		lblStockno_1.setBounds(380, 111, 80, 14);
		frame.getContentPane().add(lblStockno_1);
		
		
//-------------------------------------------TextFields----------------------------------------------------------//
		
		textFieldB_Title = new JTextField();
		textFieldB_Title.setBounds(88, 36, 86, 20);
		frame.getContentPane().add(textFieldB_Title);
		textFieldB_Title.setColumns(10);
		
		textFieldB_Author = new JTextField();
		textFieldB_Author.setBounds(88, 58, 86, 20);
		frame.getContentPane().add(textFieldB_Author);
		textFieldB_Author.setColumns(10);
		
		textFieldISBN = new JTextField();
		textFieldISBN.setBounds(88, 83, 86, 20);
		frame.getContentPane().add(textFieldISBN);
		textFieldISBN.setColumns(10);
		
		textFieldB_LibRefNo = new JTextField();
		textFieldB_LibRefNo.setBounds(88, 108, 86, 20);
		frame.getContentPane().add(textFieldB_LibRefNo);
		textFieldB_LibRefNo.setColumns(10);
			
		textFieldJ_Title = new JTextField();
		textFieldJ_Title.setBounds(482, 33, 86, 20);
		frame.getContentPane().add(textFieldJ_Title);
		textFieldJ_Title.setColumns(10);
		
		textFieldJ_Author = new JTextField();
		textFieldJ_Author.setBounds(482, 58, 86, 20);
		frame.getContentPane().add(textFieldJ_Author);
		textFieldJ_Author.setColumns(10);
						
		textFieldJ_libRefNo = new JTextField();
		textFieldJ_libRefNo.setBounds(482, 83, 86, 20);
		frame.getContentPane().add(textFieldJ_libRefNo);
		textFieldJ_libRefNo.setColumns(10);
		
		textFieldBookStkNum = new JTextField();
		textFieldBookStkNum.setBounds(88, 133, 86, 20);
		frame.getContentPane().add(textFieldBookStkNum);
		textFieldBookStkNum.setColumns(10);
		
		textFieldJStkNum = new JTextField();
		textFieldJStkNum.setBounds(482, 108, 86, 20);
		frame.getContentPane().add(textFieldJStkNum);
		textFieldJStkNum.setColumns(10);
		
//-------------------------------------------Buttons----------------------------------------------------------//	
		
		searchJAuthorButton = new JButton("Search J Author");
		searchJAuthorButton.setBounds(603, 57, 145, 23);
		frame.getContentPane().add(searchJAuthorButton);
		
		searchJRefButton = new JButton("Search J Ref");
		searchJRefButton.setBounds(603, 82, 145, 23);
		frame.getContentPane().add(searchJRefButton);
		
		searchBAuthorButton = new JButton("Search B Author");
		searchBAuthorButton.setBounds(184, 57, 145, 23);
		frame.getContentPane().add(searchBAuthorButton);
		
		searchISBNButton = new JButton("Search ISBN");
		searchISBNButton.setBounds(184, 82, 145, 23);
		frame.getContentPane().add(searchISBNButton);
		
		searchBRefButton = new JButton("Search B Ref");
		searchBRefButton.setBounds(184, 107, 145, 23);
		frame.getContentPane().add(searchBRefButton);
		
		searchBTitleButton = new JButton("Search B Title");
		searchBTitleButton.setBounds(184, 36, 145, 20);
		frame.getContentPane().add(searchBTitleButton);  
		
		searchJTitleButton = new JButton("Search J Title");
		searchJTitleButton.setBounds(603, 32, 145, 23);
		frame.getContentPane().add(searchJTitleButton);
		
		clearButton = new JButton("Clear Search");
		clearButton.setBounds(108, 211, 120, 23);
		frame.getContentPane().add(clearButton);
		
		placeRequestButton = new JButton("Place Request");
		placeRequestButton.setBounds(497, 211, 120, 23);
		frame.getContentPane().add(placeRequestButton);
		
		mainMenuButton = new JButton("Main Menu");
		mainMenuButton.setBounds(306, 211, 120, 23);
		frame.getContentPane().add(mainMenuButton);
		
		searchBTitleButton.addActionListener(this);
		searchJTitleButton.addActionListener(this);
		searchBAuthorButton.addActionListener(this);
		searchJAuthorButton.addActionListener(this);
		searchBRefButton.addActionListener(this);
		searchJRefButton.addActionListener(this);
		searchISBNButton.addActionListener(this);
		clearButton.addActionListener(this);
		placeRequestButton.addActionListener(this);
		mainMenuButton.addActionListener(this);
		
	}
	
	
//-------------------------------------------Action Listeners----------------------------------------------------------//
	
	public void actionPerformed(ActionEvent e) {
			
			SearchItem si = new SearchItem();
			Object target = e.getSource();

//-------------------------------------------search book title----------------------------------------------------------//			
			if(target == searchBTitleButton)
			{			
				String bookTitle = textFieldB_Title.getText();
				
				try {
						if (item == null){
						item = si.searchBookTitle(bookTitle);
						textFieldB_Title.setText(bookTitle);
						textFieldB_Author.setText(item.getAuthor());
						textFieldISBN.setText(String.valueOf((item.getISBN())));
						textFieldB_LibRefNo.setText(String.valueOf((item.getId())));
						textFieldBookStkNum.setText(String.valueOf((item.getStocknumber())));
						}
						
						else{
							JOptionPane.showMessageDialog(null,
							        "The book " + bookTitle + " does not exist in library.",
							        "Invalid Search.",
							        JOptionPane.INFORMATION_MESSAGE);
						}
						
												
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}
			
//-------------------------------------------search journal title----------------------------------------------------------//		
		if(target == searchJTitleButton)
		{
			String journalTitle = textFieldJ_Title.getText();
				try {
						if (item == null){
						item = si.searchJournalTitle(journalTitle);
						textFieldJ_Title.setText(journalTitle);
						textFieldJ_Author.setText(item.getAuthor());
						textFieldB_LibRefNo.setText(String.valueOf((item.getId())));
						textFieldJStkNum.setText(String.valueOf((item.getStocknumber())));;}
						else{
							JOptionPane.showMessageDialog(null,
							        "The journal " + journalTitle + " does not exist in library.",
							        "Invalid Search.",
							        JOptionPane.INFORMATION_MESSAGE);
						}
																		
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
		}

//-------------------------------------------search book author----------------------------------------------------------//			
		if (target == searchBAuthorButton)
		{
			String bookAuthor = textFieldB_Author.getText();
				try {
						if (item == null){
						item = si.searchBookAuthor(bookAuthor);
						textFieldB_Author.setText(bookAuthor);
						textFieldB_Title.setText(item.getTitle());
						textFieldISBN.setText(String.valueOf((item.getISBN())));
						textFieldB_LibRefNo.setText(String.valueOf((item.getId())));
						textFieldBookStkNum.setText(String.valueOf((item.getStocknumber())));}
						else{
							JOptionPane.showMessageDialog(null,
							        "The author " + bookAuthor + " does not exist in library.",
							        "Invalid Search.",
							        JOptionPane.INFORMATION_MESSAGE);
						}
																		
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}
		
//-------------------------------------------search journal author----------------------------------------------------------//	
		if (target == searchJAuthorButton)
		{
			String journalAuthor = textFieldJ_Author.getText();
				try {
						if (item == null){
						item = si.searchJournalAuthor(journalAuthor);
						textFieldJ_Author.setText(journalAuthor);
						textFieldJ_Title.setText(item.getTitle());
						textFieldJ_libRefNo.setText(String.valueOf((item.getId())));
						textFieldJStkNum.setText(String.valueOf((item.getStocknumber())));}
						else{
							JOptionPane.showMessageDialog(null,
							        "The author " + journalAuthor + " does not exist in library.",
							        "Invalid Search.",
							        JOptionPane.INFORMATION_MESSAGE);
						}
																		
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}
		
		//-------------------------------------------search book Id(Library Reference No)----------------------------------------------------------//			
				
		if (target == searchBRefButton)
				{
					String bookID = textFieldB_LibRefNo.getText();
						try {
								if (item == null){
								item = si.searchBookRefNo(bookID);
								textFieldB_Author.setText(item.getAuthor());
								textFieldB_Title.setText(item.getTitle());
								textFieldISBN.setText(String.valueOf((item.getISBN())));
								textFieldB_LibRefNo.setText(String.valueOf((item.getId())));
								textFieldBookStkNum.setText(String.valueOf((item.getStocknumber())));}
								else{
									JOptionPane.showMessageDialog(null,
									        "The refrence number " + bookID + " does not exist in library.",
									        "Invalid Search.",
									        JOptionPane.INFORMATION_MESSAGE);
								}
																				
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
					}
		
//-------------------------------------------search journal Id(Library Reference No)--------------------------------------------------//			
		
			if (target == searchJRefButton)
						{
							String journalID = textFieldJ_libRefNo.getText();
								try {
										if (item == null){
										item = si.searchBookRefNo(journalID);
										textFieldJ_Author.setText(item.getAuthor());
										textFieldJ_Title.setText(item.getTitle());
										textFieldJ_libRefNo.setText(String.valueOf((item.getId())));
										textFieldJStkNum.setText(String.valueOf((item.getStocknumber())));}
										else{
											JOptionPane.showMessageDialog(null,
											        "The refrence number " + journalID + " does not exist in library.",
											        "Invalid Search.",
											        JOptionPane.INFORMATION_MESSAGE);
										}
																						
									} catch (Exception e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
		}
			
			
			//-------------------------------------------search book ISBN----------------------------------------------------------//			
			
			if (target == searchISBNButton)
					{
						String bookISBN = textFieldISBN.getText();
							try {
									if (item == null){
									item = si.searchBookRefNo(bookISBN);
									textFieldB_Author.setText(item.getAuthor());
									textFieldB_Title.setText(item.getTitle());
									textFieldISBN.setText(String.valueOf((item.getISBN())));
									textFieldB_LibRefNo.setText(String.valueOf((item.getId())));
									textFieldBookStkNum.setText(String.valueOf((item.getStocknumber())));}
									else{
										JOptionPane.showMessageDialog(null,
										        "The ISBN " + bookISBN + " does not exist in library.",
										        "Invalid Search.",
										        JOptionPane.INFORMATION_MESSAGE);
									}
																					
								} catch (Exception e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
						}
						
//-------------------------------------------clear textFields----------------------------------------------------------//				
		if (target == clearButton){
			textFieldB_Title.setText(" ");
			textFieldB_Author.setText(" ");
			textFieldISBN.setText(String.valueOf((" ")));
			textFieldB_LibRefNo.setText(String.valueOf((" ")));
			textFieldJ_Title.setText(" ");
			textFieldJ_Author.setText(" ");
			textFieldB_LibRefNo.setText(String.valueOf((" ")));
			textFieldBookStkNum.setText(String.valueOf((" ")));
		}
 
//-------------------------------------------place request----------------------------------------------------------//					
		if (target == placeRequestButton){
			GUIPlaceReq window = new GUIPlaceReq(user);
			frame.setVisible(false);
		}

//-------------------------------------------main menu----------------------------------------------------------//			
		if (target == mainMenuButton){
			//LibAdminGui window = new LibAdminGui();
			frame.setVisible(false);
		}
		
		}
}



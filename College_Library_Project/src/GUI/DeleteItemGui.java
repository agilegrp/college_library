package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

import model.User;
import controller.CancelReq;
import controller.DeleteItem;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DeleteItemGui {

	static User user;
	public JFrame frame;
	private JTextField TFid;
	String tetFid;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//GUICancelReq window = new GUICancelReq();
					DeleteItemGui window = new DeleteItemGui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public DeleteItemGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 362, 182);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		TFid = new JTextField();
		TFid.setBounds(144, 27, 121, 20);
		frame.getContentPane().add(TFid);
		TFid.setColumns(10);
		
		JLabel lblRefernceId = new JLabel("Refernce ID");
		lblRefernceId.setBounds(29, 30, 111, 14);
		frame.getContentPane().add(lblRefernceId);
		
		JButton btnDeleteItem = new JButton("Delete Item");
		btnDeleteItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				DeleteItem di = new DeleteItem();
				String ref = "";
	 		
				if(TFid.getText().equals(""))
		 		{
		 			JOptionPane.showMessageDialog(null,
					        "Invalid data entered/ enter ID ", 
							"Delete Item", 
							JOptionPane.PLAIN_MESSAGE);
		 		}else {
		 			tetFid = TFid.getText();
				
				try {
						ref=di.getref(tetFid);
				
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			if (ref == "Invalid Entry"){
				JOptionPane.showMessageDialog(null,"Error : "+ref,"Note!",
		 		        JOptionPane.INFORMATION_MESSAGE);
			}else
			JOptionPane.showMessageDialog(null,"ID "+ref+" Item Deleted","Note!",
		 		        JOptionPane.INFORMATION_MESSAGE);
			}
			}
		});
		btnDeleteItem.setBounds(29, 85, 138, 23);
		frame.getContentPane().add(btnDeleteItem);
		
		JButton btnBackToMenu = new JButton("Back To Menu");
		btnBackToMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);

			}
		});
		btnBackToMenu.setBounds(177, 85, 138, 23);
		frame.getContentPane().add(btnBackToMenu);
	}
}

package GUI;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import controller.FineAdmin;
import controller.ViewReq;
import controller.loanAdmin;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JRadioButton;
import Doa.LibraryDao;
import controller.FineAdmin;

public class LoanReturnGUI {

	private JFrame frame;
	private JTextField email_TF;
	private JTextField userid_TF;
	private JTextField fdate;
	private JTextField testFtitle;
	private JTextField textFstart;
	private JTextField textFend;

	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoanReturnGUI window = new LoanReturnGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoanReturnGUI() {
	
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setBackground(Color.CYAN);
		frame.getContentPane().setBackground(Color.CYAN);
		
		JLabel lblPlaceRequest = new JLabel("Loan Return ");
		lblPlaceRequest.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblPlaceRequest.setBounds(138, 11, 204, 23);
		frame.getContentPane().add(lblPlaceRequest);
		
		JLabel lblTitle = new JLabel("Title");
		lblTitle.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblTitle.setBounds(31, 74, 86, 14);
		frame.getContentPane().add(lblTitle);
		
		testFtitle = new JTextField();
		testFtitle.setBounds(125, 71, 137, 23);
		frame.getContentPane().add(testFtitle);
		testFtitle.setColumns(10);
		
		JLabel lblIsbn = new JLabel("Email");
		lblIsbn.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblIsbn.setBounds(31, 102, 86, 14);
		frame.getContentPane().add(lblIsbn);
		
		email_TF = new JTextField();
		email_TF.setColumns(10);
		email_TF.setBounds(125, 105, 137, 23);
		frame.getContentPane().add(email_TF);
		
		JButton button = new JButton("Back to Menu");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
		
			}
		});
		button.setBounds(193, 161, 117, 23);
		frame.getContentPane().add(button);
		
		JButton loanReq = new JButton("Process Return");
		loanReq.setBounds(61, 161, 127, 23);
		frame.getContentPane().add(loanReq);

		
	
		//-----------------------------------------------------------------Check action listner *************
		loanReq.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loanAdmin loan0 = new loanAdmin();
				
	 		if(testFtitle.getText().equals("") || email_TF.getText().equals(""))
	 		{
	 			JOptionPane.showMessageDialog(null,
				        "Invalid data entered ", 
						"Loan Administration", 
						JOptionPane.PLAIN_MESSAGE);
	 		}
	 		else{

		 		String emailId = email_TF.getText();
		 		String bookTitle = testFtitle.getText();
		 		String result = "";
		 		//int stPage = Integer.parseInt(textFstart.getText());
		 		//int stopPage = Integer.parseInt(textFstart.getText());
		 		
		 		ResultSet rs = null;
				String cmd = "select * from user where email = '"+emailId+"'";
				int userId = -1;
				try {
					rs = new LibraryDao().queryDB(cmd);
					
					if (rs.next())
					{
						userId = rs.getInt("USER_ID");
						
						//result = new FineAdmin().checkMyLoans(userId);
						
						Double res = loan0.loanReturn(emailId, bookTitle);
						if (res == 0.0)
						{	
							JOptionPane.showMessageDialog(null, 
									"Loan Returned", 
									"Loan Administration", 
									JOptionPane.PLAIN_MESSAGE);
						}
						else
						{
							JOptionPane.showMessageDialog(null, 
									"User has Fines pending \n ", 
									"Loan Administration", 
									JOptionPane.PLAIN_MESSAGE);
						}
					}
					else
					{
						JOptionPane.showMessageDialog(null, 
								"Email Id does not exist", 
								"Loan Administration", 
								JOptionPane.PLAIN_MESSAGE);
					}
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
	 		} }});
		
		frame.setSize(400, 250);
		frame.setVisible(true);
	}
}

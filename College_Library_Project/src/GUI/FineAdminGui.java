package GUI;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.FineAdmin;

public class FineAdminGui implements ActionListener 
{
	private JButton btnUpdateBks;
	private JButton btnViewRate;
	private JTextField lbPassTF;
	private JTextField lbAmountTF;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
			 		FineAdminGui window = new FineAdminGui();
					//window.frame.setVisible(true);
				} catch (Exception e) {
		 			e.printStackTrace();
				}
			}
		});
	}

	public FineAdminGui() 
	{
		JFrame frame = new JFrame();
		frame.setTitle("Project Group B3");
		Container cp = frame.getContentPane();

		final CardLayout cards = new CardLayout();

		// *********************************Main
		// Screen***************************

		final JPanel fineAdmin = new JPanel();
		fineAdmin.setLayout(new FlowLayout());
		final JPanel Box1 = new JPanel();
		Box1.setBackground(Color.CYAN);
		final JPanel Box2 = new JPanel();
		Box2.setBackground(Color.CYAN);
		Box1.setLayout(new GridLayout(2, 2));
		Box2.setLayout(new BoxLayout(Box2, BoxLayout.Y_AXIS));

		JLabel Mainlabel = new JLabel("Fine Administration");
		JLabel lbPass = new JLabel(" Password   ");

		lbPassTF = new JTextField();
		lbPassTF.setColumns(5);
		JLabel lbAmount = new JLabel(" New Fine Rate      ");
		lbAmountTF = new JTextField();

		btnUpdateBks = new JButton("Update Fine Rate");
		btnUpdateBks.setSize(80, 30);
		btnViewRate = new JButton ("  View Fine Rate  ");
		btnViewRate.setSize(110, 30);
		Mainlabel.setFont(new Font("Serif", Font.PLAIN, 28));

		fineAdmin.add(Mainlabel, BorderLayout.PAGE_START);
		fineAdmin.setBackground(Color.CYAN);
		Box1.add(lbPass);
		Box1.add(lbPassTF);
		Box1.add(lbAmount);
		Box1.add(lbAmountTF);
		Box2.add(btnUpdateBks);
		Box2.add(btnViewRate);

		btnUpdateBks.addActionListener(this);
		btnViewRate.addActionListener(this);

		fineAdmin.add(Box1);
		fineAdmin.add(Box2);

		// ----------------------------------------------------------------

		final JPanel cardPanel = new JPanel();
		cardPanel.setBackground(Color.YELLOW);
		cardPanel.setLayout(cards);

		// ----------------------------------------------------------------

		cardPanel.add(fineAdmin, "FineAdmin");
		cp.add(cardPanel);
		frame.pack();
		cp.setBackground(Color.LIGHT_GRAY);
		frame.setSize(350, 200);
		frame.setVisible(true);
	}//End Constructor

	// --------------ActionListeners-------------------------

	public void actionPerformed(ActionEvent e) // listens to buttons
	{
		FineAdmin fa = new FineAdmin();

 		Object target = e.getSource();
		if (target == btnUpdateBks) 
		{
			String pass = lbPassTF.getText();
	 		Double amt = Double.parseDouble(lbAmountTF.getText());
			Boolean res = fa.updateFineRate("admin1",pass, amt);
		}
	 	if (target == btnViewRate) 
		{
			try 
			{
				String rate = fa.viewFineRate();
				JOptionPane.showMessageDialog(null,
				        "The current fine rate is " + rate + " cents per day",
				        "Current Fine Rate",
				        JOptionPane.INFORMATION_MESSAGE);
			
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	 		
		}
	}//end ActionListenerMethod
	
	public String overdueMsg(String message)
	{
		JOptionPane.showMessageDialog(null,
		         message,
		        "Notification",
 		        JOptionPane.INFORMATION_MESSAGE);
		
		return "Notification Sent";
	}

}//End Class

package GUI;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.User;
import controller.DeleteItem;
import controller.FineAdmin;

public class LibAdminGui implements ActionListener 
{
	private JButton btnRecLoan;
	private JButton btnRecReturn;
	private JButton btnSetFineRate;
	private JButton btnSearch;
	private JButton btnViewReq;
	private JButton btnPlaceReq;
	private JButton btnAddBook;
	private JButton btnAddJournal;
	private JButton btnDelItem;
	private JButton btnCheckFines;
	private JButton btnChkMyLoans;
	private JButton btnLogout;
	private User user;
	private JFrame frame;
	

	public LibAdminGui(User usr)  
	{
		this.user = usr;
		frame = new JFrame();
		frame.setTitle("Project Group B3");
		Container cp = frame.getContentPane();

		final CardLayout cards = new CardLayout();

		// *********************************Main
		// Screen***************************

		final JPanel libAdmin = new JPanel();
		libAdmin.setLayout(new FlowLayout());
		final JPanel Box2 = new JPanel();
		Box2.setBackground(Color.CYAN);
		Box2.setLayout(new GridLayout(13,2));

		JLabel Mainlabel = new JLabel("Library Administration");
		JLabel lbAdmin = new JLabel(" Administrator Functions");


		btnSearch =      new JButton ("      Search      ");
		btnViewReq =     new JButton ("View Request   ");
		btnPlaceReq =    new JButton ("Place Request  ");
		btnChkMyLoans  = new JButton (" Check My Loans");
		btnRecLoan =     new JButton (" Record Loan");
		btnRecReturn =   new JButton (" Record Return");
		btnAddBook =     new JButton (" Add New Book");
		btnAddJournal =  new JButton ("Add New Journal");
		btnDelItem    =  new JButton ("Delete Item");
		btnSetFineRate = new JButton (" Set Fine Rate  ");
		btnCheckFines = new JButton (" Check Fines");
		btnLogout	   = new JButton ("    Logout  ");
		
		
		btnRecLoan.setSize(110, 30);
		btnRecReturn.setSize(110, 30);
		btnSetFineRate.setSize(110, 30);
		btnSearch.setSize(110, 30);
		btnViewReq.setSize(110, 30);
		btnPlaceReq.setSize(110, 30);
		btnDelItem.setSize(110, 30);
		Mainlabel.setFont(new Font("Serif", Font.PLAIN, 28));
		lbAdmin.setFont(new Font("Serif", Font.PLAIN, 16));

		libAdmin.add(Mainlabel, BorderLayout.PAGE_START);
		libAdmin.setBackground(Color.CYAN);

		Box2.add(btnSearch);
		Box2.add(btnViewReq);
		Box2.add(btnPlaceReq);
		Box2.add(btnChkMyLoans);
		Box2.add(lbAdmin);
		Box2.add(btnRecLoan);
		Box2.add(btnRecReturn);
		Box2.add(btnAddBook);
		Box2.add(btnAddJournal);
		Box2.add(btnDelItem);
		Box2.add(btnSetFineRate);
		Box2.add(btnCheckFines);
		Box2.add(btnLogout);
		

		btnRecLoan.addActionListener(this);
		btnRecReturn.addActionListener(this);
		btnSetFineRate.addActionListener(this);
		btnSearch.addActionListener(this);
		btnAddBook.addActionListener(this);
		btnAddJournal.addActionListener(this);
		btnDelItem.addActionListener(this);
		btnViewReq.addActionListener(this);
		btnPlaceReq.addActionListener(this);
		btnCheckFines.addActionListener(this);
		btnChkMyLoans.addActionListener(this);
		btnLogout.addActionListener(this);
		
		if (user.getId() == 0)
		{
			lbAdmin.setVisible(true);
			btnRecLoan.setVisible(true);
			btnRecReturn.setVisible(true);
			btnSetFineRate.setVisible(true);
			btnAddBook.setVisible(true);
			btnAddJournal.setVisible(true);
			btnDelItem.setVisible(true);
			btnCheckFines.setVisible(true);
		}
		else
		{
			lbAdmin.setVisible(false);
			btnRecLoan.setVisible(false);
			btnRecReturn.setVisible(false);
			btnSetFineRate.setVisible(false);
			btnAddBook.setVisible(false);
			btnAddJournal.setVisible(false);
			btnDelItem.setVisible(false);
			btnCheckFines.setVisible(false);
		}
		libAdmin.add(Box2);

		// ----------------------------------------------------------------

		final JPanel cardPanel = new JPanel();
		cardPanel.setBackground(Color.YELLOW);
		cardPanel.setLayout(cards);

		// ----------------------------------------------------------------

		cardPanel.add(libAdmin, "LibAdmin");
		cp.add(cardPanel);
		frame.pack();
		cp.setBackground(Color.LIGHT_GRAY);
		frame.setSize(350, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}//End Constructor

	// --------------ActionListeners-------------------------

	public void actionPerformed(ActionEvent e) // listens to buttons
	{
		FineAdmin fa = new FineAdmin();

 		Object target = e.getSource();
		if (target == btnRecLoan) 
		{
			LoanAdimGUI window = new LoanAdimGUI();
		}
	 	if (target == btnRecReturn) 
		{
	 		LoanReturnGUI window = new LoanReturnGUI();
	 		
		}
	 	if (target == btnSearch) 
		{
	 		SearchGui window = new SearchGui();
		}
	 	if (target == btnAddBook) 
		{
	 		BookGui window = new BookGui();
	 		EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						BookGui window = new BookGui();
						window.frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
		
	 	if (target == btnAddJournal) 
		{
	 		JournalsGui window = new JournalsGui();
	 		EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						JournalsGui window = new JournalsGui();
						window.frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
	 	if (target == btnDelItem) 
		{
	 		DeleteItemGui window = new DeleteItemGui();
	 		window.frame.setVisible(true);
		}
		
	 	if (target == btnViewReq) 
		{
	 		ViewGUI window = new ViewGUI(user);
		}
	 	if (target == btnPlaceReq) 
		{
	 		GUIPlaceReq window = new GUIPlaceReq(user);
		}
	 	if (target == btnSetFineRate) 
		{
	 		FineAdminGui window = new FineAdminGui();
		}
	 	if (target == btnCheckFines) 
		{
	 		chkFines();
		}
	 	if (target == btnChkMyLoans) 
		{
	 		chkMyLoans(user);
		}
	 	if (target == btnLogout) 
		{
	 		frame.setVisible(false);
	 		System.exit(0);
		}
	}//end ActionListenerMethod
	
 public void chkFines()
 {
	 FineAdmin cf = new FineAdmin();
	 try {
		String result = cf.checkFines();
		
		JOptionPane.showMessageDialog(null,
		        result,
	 	        "Fines Check",
		        JOptionPane.INFORMATION_MESSAGE);
	} catch (Exception e) 
	 {
		e.getMessage();
	}
 }
 
 public void chkMyLoans(User user)
 {
	 FineAdmin cf = new FineAdmin();
	 try {
		String result = cf.checkMyLoans(user.getId());
		
		JOptionPane.showMessageDialog(null,
		        result,
	 	        "Fines Check",
		        JOptionPane.INFORMATION_MESSAGE);
	} catch (Exception e) 
	 {
		e.getMessage();
	}
 }

}//End Class

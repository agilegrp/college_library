package GUI;


import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class GUIMain {
public static void main(String[] args) {
JFrame frame = new JFrame();
frame.setTitle("Project Group B3");
Container cp = frame.getContentPane();
//cp.setLayout(new GridLayout(4));
//cp.setLayout(new GridBagLayout());

final CardLayout cards = new CardLayout();


//*********************************Main Screen***************************

final JPanel MainS = new JPanel();
MainS.setLayout(new GridLayout(3,2));
final JPanel Box1 = new JPanel();
final JPanel Box2 = new JPanel();

Box1.setLayout(new BoxLayout(Box1, BoxLayout.X_AXIS));
Box2.setLayout(new GridLayout(9,2));


JLabel Mainlabel = new JLabel("Home Screen");
JLabel l0 = new JLabel("Describe what the button is going to do when pressed ");
JLabel l1 = new JLabel("Home          Label");
JLabel l2 = new JLabel("Home          Label");
JLabel l3 = new JLabel("Home           Label");
JLabel l4 = new JLabel("Home           Label");
JLabel l5 = new JLabel("Home            Label");
JButton b0 = new JButton ("Click Here for Register(example)");
JButton b1 = new JButton("JBUTTON ");
JButton b2 = new JButton ("JBUTTON  ");
JButton b3 = new JButton ("JBUTTON ");
JButton b4 = new JButton ("JBUTTON ");
JButton b5 = new JButton ("JBUTTON ");
JButton adminb = new JButton("Admin");
JButton butExit = new JButton("Exit");
Mainlabel.setFont(new Font("Serif", Font.PLAIN, 28));



MainS.add(Mainlabel,BorderLayout.PAGE_START);
MainS.setBackground(Color.CYAN);
Box2.add(l0);
Box2.add(b0);
Box2.add(l1);
Box2.add(b1);
Box2.add(l2);
Box2.add(b2);
Box2.add(l3);
Box2.add(b3);
Box2.add(l4);
Box2.add(b4);
Box2.add(l5);
Box2.add(b5);
Box1.add(adminb);
Box1.add(butExit);

MainS.add(Box2);
MainS.add(Box1);
//MainS.setLayout(new GridLayout(4,2));
Box2.setLayout(new GridLayout(6,2));

//----------------------------------------------------------------

final JPanel cardPanel = new JPanel();
cardPanel.setBackground(Color.YELLOW);
cardPanel.setLayout(cards);

//----------------------------------------------------------------

cardPanel.add(MainS, "MainGUI");


//   --------------ActionListeners-------------------------

ActionListener alExit = new ActionListener() {
public void actionPerformed(ActionEvent e) {
System.exit(0);

}
};
butExit.addActionListener(alExit);



cp.add(cardPanel);
frame.pack();
cp.setBackground(Color.LIGHT_GRAY);
frame.setSize(700,400);
frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
frame.setVisible(true);
}


}


package GUI;

import java.awt.EventQueue;	




import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JTextField;

import Doa.LibraryDao;
import controller.AddItem;
import controller.UpdateItem;
import model.Item;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.awt.event.ActionEvent;

public class BookGui {

	public JFrame frame;
	private JTextField textField;
	private JTextField textField1;
	private JTextField textField2;
	private JTextField textField3;
	private JTextField textField4;
	private JTextField textField5;
	
	private JTextField textField6;
	private JTextField textField7;
	private JTextField textField8;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BookGui window = new BookGui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public BookGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.CYAN);
		frame.setBounds(100, 50, 296, 534);
		
		frame.getContentPane().setLayout(null);
		
		JLabel AddBooks = new JLabel(" Books");
		AddBooks.setForeground(Color.BLACK);
		AddBooks.setBounds(42, 13, 84, 31);
		frame.getContentPane().add(AddBooks);
		
		JLabel Title = new JLabel("Title:");
		Title.setBounds(24, 111, 72, 18);
		frame.getContentPane().add(Title);
		
		JLabel Subject = new JLabel("Subject:");
		Subject.setBounds(24, 142, 62, 21);
		frame.getContentPane().add(Subject);
		
		JLabel Author = new JLabel("Author:");
		Author.setBounds(24, 181, 72, 18);
		frame.getContentPane().add(Author);
		
		JLabel Publisher = new JLabel("Publisher:");
		Publisher.setBounds(24, 213, 72, 28);
		frame.getContentPane().add(Publisher);
		
		JLabel Genre = new JLabel("Genre:");
		Genre.setBounds(24, 254, 72, 18);
		frame.getContentPane().add(Genre);
		
		JLabel ISBN = new JLabel("ISBN:");
		ISBN.setBounds(24, 285, 72, 36);
		frame.getContentPane().add(ISBN);
		
		textField = new JTextField();
		textField.setBounds(109, 77, 86, 24);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField1 = new JTextField();
		textField1.setBounds(109, 109, 86, 24);
		frame.getContentPane().add(textField1);
		textField1.setColumns(10);
		
		textField2 = new JTextField();
		textField2.setBounds(110, 142, 86, 24);
		frame.getContentPane().add(textField2);
		textField2.setColumns(10);
		
		textField3 = new JTextField();
		textField3.setBounds(109, 178, 86, 24);
		frame.getContentPane().add(textField3);
		textField3.setColumns(10);
		
		textField4 = new JTextField();
		textField4.setBounds(109, 215, 86, 24);
		frame.getContentPane().add(textField4);
		textField4.setColumns(10);
		
		textField5 = new JTextField();
		textField5.setBounds(109, 252, 86, 24);
		frame.getContentPane().add(textField5);
		textField5.setColumns(10);
		
		
		textField6 = new JTextField();
		textField6.setBounds(109, 289, 86, 24);
		frame.getContentPane().add(textField6);
		textField6.setColumns(10);
		
		JLabel Id = new JLabel("ID:");
		Id.setBounds(24, 80, 72, 18);
		frame.getContentPane().add(Id);
		
		
		JButton btnAddBooks = new JButton("Add Books");
	
		
		btnAddBooks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddItem item= new AddItem();
				
				int id = Integer.parseInt(textField.getText());
				String title =textField1.getText();
				String subject =textField2.getText();
				String author =textField3.getText();
				
				String publisher =textField4.getText();
				String genre =textField5.getText();
				int ISBN = Integer.parseInt(textField6.getText());
				int numloans =Integer.parseInt(textField7.getText());
				
				int stocknumber =Integer.parseInt(textField8.getText());
			
				
				try {
					item.saveItem(id,title,subject,author,publisher,genre,ISBN,numloans,stocknumber);
				}
				catch (Exception f)
				{
					f.printStackTrace();
				}
					
				}
			
				
				
			
			
		});
		btnAddBooks.setBounds(10, 421, 113, 27);
		frame.getContentPane().add(btnAddBooks);
		
		
		JButton btnupdatebooks = new JButton("Update Books");
		
		
		btnupdatebooks.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				
				int ITEM_ID = Integer.parseInt(textField.getText());
				String title =textField1.getText();
				String subject =textField2.getText();
				String author =textField3.getText();
				
				String publisher =textField4.getText();
				String genre =textField5.getText();
				int ISBN = Integer.parseInt(textField6.getText());
				int numloans =Integer.parseInt(textField7.getText());
				
				int stocknumber =Integer.parseInt(textField8.getText());
			
				UpdateItem item=new UpdateItem();
				try {
					item.updateItem(ITEM_ID, title, subject, author, publisher, genre, ISBN, numloans, stocknumber);
				}
				catch (Exception f)
				{
					f.printStackTrace();
				}
					
				}
			
				
				
				
					
				});
		btnupdatebooks.setBounds(170, 421, 113, 27);
				frame.getContentPane().add( btnupdatebooks);
				
				
		JLabel Numloans= new JLabel("Numloans:");
		Numloans.setBounds(24, 320, 72, 18);
		frame.getContentPane().add(Numloans);
		
		textField7 = new JTextField();
		textField7.setBounds(109, 320, 86, 24);
		frame.getContentPane().add(textField7);
		textField7.setColumns(10);
		
		JLabel Stocknumber = new JLabel("Stocknumber:");
		Stocknumber.setBounds(24, 360, 72, 18);
		frame.getContentPane().add(Stocknumber);
		
		textField8 = new JTextField();
		textField8.setBounds(109, 360, 86, 24);
		frame.getContentPane().add(textField8);
		textField8.setColumns(10);
	}
}
package GUI;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import controller.FineAdmin;
import controller.ViewReq;
import controller.loanAdmin;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JRadioButton;
import Doa.LibraryDao;
import controller.FineAdmin;

public class LoanAdimGUI {

	private JFrame frame;
	private JTextField femail;
	private JTextField fuserid;
	private JTextField fdate;
	private JTextField testFtitle;
	private JTextField textFstart;
	private JTextField textFend;

	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoanAdimGUI window = new LoanAdimGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoanAdimGUI() {
	
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setBackground(Color.CYAN);
		
		JButton loanReq = new JButton("Process Loan");
		loanReq.setBounds(31, 391, 117, 23);
		frame.getContentPane().add(loanReq);
		
		testFtitle = new JTextField();
		testFtitle.setBounds(125, 111, 137, 23);
		frame.getContentPane().add(testFtitle);
		testFtitle.setColumns(10);
		
		femail = new JTextField();
		femail.setColumns(10);
		femail.setBounds(125, 145, 137, 23);
		frame.getContentPane().add(femail);
		
		JLabel lblTitle = new JLabel("Title");
		lblTitle.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblTitle.setBounds(31, 114, 86, 14);
		frame.getContentPane().add(lblTitle);
		
		JLabel lblIsbn = new JLabel("Email");
		lblIsbn.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblIsbn.setBounds(31, 152, 86, 14);
		frame.getContentPane().add(lblIsbn);
		
		JButton button = new JButton("Back to Menu");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
		
			}
		});
		button.setBounds(163, 391, 117, 23);
		frame.getContentPane().add(button);
		
		JLabel lblStartPage = new JLabel("Start Page");
		lblStartPage.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblStartPage.setBounds(31, 210, 86, 23);
		frame.getContentPane().add(lblStartPage);
		
		JLabel lblEndPage = new JLabel("End Page");
		lblEndPage.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblEndPage.setBounds(31, 244, 86, 20);
		frame.getContentPane().add(lblEndPage);
		
		textFstart = new JTextField();
		textFstart.setText("0");
		textFstart.setColumns(10);
		textFstart.setBounds(125, 211, 137, 23);
		frame.getContentPane().add(textFstart);
		
		textFend = new JTextField();
		textFend.setText("0");
		textFend.setColumns(10);
		textFend.setBounds(125, 241, 137, 23);
		frame.getContentPane().add(textFend);

		JRadioButton rdbtnBook = new JRadioButton("Book");
		JRadioButton rdbtnJournals = new JRadioButton("Journals");
		rdbtnJournals.setSelected(true);
		rdbtnBook.setBounds(79, 41, 67, 23);
		rdbtnJournals.setBounds(153, 41, 109, 23);


        ButtonGroup group = new ButtonGroup();

		group.add(rdbtnBook);		
		group.add(rdbtnJournals);
		
		frame.getContentPane().add(rdbtnBook);
		frame.getContentPane().add(rdbtnJournals);
		
		JLabel lblPlaceRequest = new JLabel("Loan Request ");
		lblPlaceRequest.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblPlaceRequest.setBounds(98, 11, 204, 23);
		frame.getContentPane().add(lblPlaceRequest);
		
		JLabel lblVisableOnlyFor = new JLabel("-- Next fields only Visable only for Journals");
		lblVisableOnlyFor.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		lblVisableOnlyFor.setBounds(31, 185, 231, 14);
		frame.getContentPane().add(lblVisableOnlyFor);
		
		JLabel lblInputTitleOr = new JLabel("-- Input Title OR ISBN");
		lblInputTitleOr.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		lblInputTitleOr.setBounds(79, 86, 133, 14);
		frame.getContentPane().add(lblInputTitleOr);

		rdbtnJournals.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 textFstart.setEnabled(true);
				 textFend.setEnabled(true);
			}
		});
		
		rdbtnBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				 textFstart.setEnabled(false);
				 textFend.setEnabled(false);
			}
		});
		

		//-----------------------------------------------------------------Check action listner *************
		loanReq.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loanAdmin loan0 = new loanAdmin();
				
	 		if(testFtitle.getText().equals("") || textFend.getText().equals(""))
	 		{
	 			JOptionPane.showMessageDialog(null,
				        "Invalid data entered ", 
						"Loan Administration", 
						JOptionPane.PLAIN_MESSAGE);
	 		}
	 		else{

		 		String emailId = femail.getText();
		 		String bookTitle = testFtitle.getText();
		 		String result = "";
		 		int stPage = Integer.parseInt(textFstart.getText());
		 		int stopPage = Integer.parseInt(textFstart.getText());
		 		
		 		ResultSet rs = null;
				String cmd = "select * from user where email = '"+emailId+"'";
				int userId = -1;
				try {
					rs = new LibraryDao().queryDB(cmd);
					
					if (rs.next())
					{
						userId = rs.getInt("USER_ID");
						
						result = new FineAdmin().checkMyLoans(userId);
						if (result.equals("No Fines Pending"))
						{
							Boolean ins = loan0.insertNewLoan(emailId, bookTitle, stPage, stopPage);
							
							JOptionPane.showMessageDialog(null, 
									"Loan Recorded", 
									"Loan Administration", 
									JOptionPane.PLAIN_MESSAGE);
						}
						else
						{
							JOptionPane.showMessageDialog(null, 
									"User has Fines pending \n "
									+ "Loan not processed", 
									"Loan Administration", 
									JOptionPane.PLAIN_MESSAGE);
						}
					}
					else
					{
						JOptionPane.showMessageDialog(null, 
								"Email Id does not exist", 
								"Loan Administration", 
								JOptionPane.PLAIN_MESSAGE);
					}
					

				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
				
	 		
	 		} }});
		
		frame.setSize(400, 500);
		frame.setVisible(true);
	}
}

package GUI;

import java.awt.BorderLayout;	 
import java.awt.CardLayout;
import java.awt.Color;

import java.awt.EventQueue;

import java.awt.Font;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


import javax.swing.JTextField;

import controller.FineAdmin;
import controller.Register;
import controller.UserLogin;
import model.User;



public class LoginGUI {
 
	private JFrame frmUserLogin;
	private JTextField textUserName;
	private JTextField textPassword;
	private JTextField textRegUserName;
	private JTextField textRegPassword;
	private JTextField textRegPhoneNumber;
	private JTextField textRegEmail;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginGUI window = new LoginGUI();
					window.frmUserLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmUserLogin = new JFrame();
		frmUserLogin.setVisible(true);
		frmUserLogin.setTitle("User Login");
		frmUserLogin.getContentPane().setBackground(Color.CYAN);
		frmUserLogin.getContentPane().setLayout(new BorderLayout(0, 0));

		final CardLayout cards = new CardLayout();
		final JPanel CardPanel = new JPanel();
		frmUserLogin.getContentPane().add(CardPanel);
		CardPanel.setLayout(cards);

		JPanel cardLogin = new JPanel();
		cardLogin.setBackground(Color.CYAN);
		CardPanel.add(cardLogin, "Panel3");
		cardLogin.setLayout(null);

		JLabel lblUserLogin = new JLabel("User Login");
		lblUserLogin.setBounds(180, 5, 74, 23);
		lblUserLogin.setFont(new Font("Serif", Font.BOLD, 16));
		cardLogin.add(lblUserLogin);

		JLabel lblUserName = new JLabel("User Name:");
		lblUserName.setFont(new Font("Serif", Font.BOLD, 14));
		lblUserName.setBounds(72, 55, 74, 15);
		cardLogin.add(lblUserName);

		textUserName = new JTextField();
		textUserName.setBounds(180, 52, 131, 21);
		cardLogin.add(textUserName);
		textUserName.setColumns(10);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Serif", Font.BOLD, 14));
		lblPassword.setBounds(72, 104, 74, 15);
		cardLogin.add(lblPassword);

		textPassword = new JTextField();
		textPassword.setBounds(180, 101, 131, 21);
		cardLogin.add(textPassword);
		textPassword.setColumns(10);

		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UserLogin uo= new UserLogin();
				String username=textUserName.getText();
				String password=textPassword.getText();
				User user;
				try {
					user = uo.userLogin(username, password);
					try{
						if(user !=null){
							LibAdminGui window = new LibAdminGui(user);
							//					JOptionPane.showMessageDialog(null,
							//					        "Successful!!",
							//					        "User Login",
							//					        JOptionPane.INFORMATION_MESSAGE);
						}else{
							JOptionPane.showMessageDialog(null,
									"Wrong User Information!!",
									"User Login",
									JOptionPane.WARNING_MESSAGE);
						}
					}catch (Exception e1) {
						e1.printStackTrace();
						
					}
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
			}

		});
		btnLogin.setBounds(53, 153, 93, 23);
		cardLogin.add(btnLogin);

		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cards.show(CardPanel,"Panel2");
			}
		});
		btnRegister.setBounds(161, 153, 93, 23);
		cardLogin.add(btnRegister);

		JButton btnGetBack = new JButton("Get Back");
		btnGetBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String username=textUserName.getText();
				UserLogin gb = new UserLogin();
				 try {
					String result = gb.getBackPassword(username);
					JOptionPane.showMessageDialog(null,
							result,
				 	        "the password has sent to the email",
					        JOptionPane.INFORMATION_MESSAGE);
				} catch (Exception e1) 
				 {
					e1.getMessage();
				}
			}
		});
		btnGetBack.setBounds(264, 153, 93, 23);
		cardLogin.add(btnGetBack);


		JPanel cardRegister = new JPanel();
		cardRegister.setBackground(Color.CYAN);
		CardPanel.add(cardRegister, "Panel2");
		cardRegister.setLayout(null);

		JLabel lblNewLabel = new JLabel("User Register");
		lblNewLabel.setBounds(175, 5, 94, 23);
		lblNewLabel.setFont(new Font("Serif", Font.BOLD, 16));
		cardRegister.add(lblNewLabel);

		JLabel lblUserI = new JLabel("User Name:");
		lblUserI.setBounds(69, 52, 106, 15);
		cardRegister.add(lblUserI);

		JLabel lblPassword_1 = new JLabel("Password:");
		lblPassword_1.setBounds(69, 77, 106, 15);
		cardRegister.add(lblPassword_1);

		JLabel lblPhoneNumber = new JLabel("Phone Number:");
		lblPhoneNumber.setBounds(69, 102, 106, 15);
		cardRegister.add(lblPhoneNumber);

		JLabel lblEmail = new JLabel("E-mail:");
		lblEmail.setBounds(69, 127, 54, 15);
		cardRegister.add(lblEmail);

		textRegUserName = new JTextField();
		textRegUserName.setBounds(203, 49, 106, 21);
		cardRegister.add(textRegUserName);
		textRegUserName.setColumns(10);

		textRegPassword = new JTextField();
		textRegPassword.setBounds(203, 74, 106, 21);
		cardRegister.add(textRegPassword);
		textRegPassword.setColumns(10);

		textRegPhoneNumber = new JTextField();
		textRegPhoneNumber.setBounds(203, 99, 106, 21);
		cardRegister.add(textRegPhoneNumber);
		textRegPhoneNumber.setColumns(10);

		textRegEmail = new JTextField();
		textRegEmail.setBounds(203, 124, 106, 21);
		cardRegister.add(textRegEmail);
		textRegEmail.setColumns(10);

		JButton btnRegister_1 = new JButton("Register");
		btnRegister_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//need code to insert information into database
				Register re=new Register();
				String userName=textRegUserName.getText();
				String password=textRegPassword.getText();
				String phoneNumber=textRegPhoneNumber.getText();
				String email=textRegEmail.getText();
//				User user= new User();
//				user.setUserName(userName);
//				user.setPassword(password);
//				user.setEmail(email);
//				user.setPhoneNumber(phoneNumber);
				try{
					re.saveUser(userName,password,email,phoneNumber);
				}catch (Exception f) {
					f.printStackTrace();
				}
			}

		});
		btnRegister_1.setBounds(83, 164, 93, 23);
		cardRegister.add(btnRegister_1);

		JButton btnBack_1 = new JButton("Back");
		btnBack_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cards.show(CardPanel, "Panel3");
			}
		});
		btnBack_1.setBounds(203, 164, 93, 23);
		cardRegister.add(btnBack_1);
		frmUserLogin.setBounds(100, 100, 450, 311);
	}
}

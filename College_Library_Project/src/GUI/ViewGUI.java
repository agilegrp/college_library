package GUI;


import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import controller.FineAdmin;
import controller.ViewReq;
import model.User;
public class ViewGUI  implements ActionListener 
{
  private JButton MreqB;
  private JButton MreqJ;
  private JButton submit;
  private JButton update;
  private JTextField bookreq;
  private JTextField journalreq;
  private JTextArea favarea0;
  private JButton clear;
  public User user;
  private JTextField fav0t;
  private JTextField fav1t;
  private JTextField fav2t;
  private JTextField fav3t;
  private JTextField fav4t;
  private JTextField restf;
  private CardLayout cards;
  private JPanel cardPanel;
  private JButton butadmin;
  private JButton butfav;
  private JButton butHome;
  private JButton butHomeG;

//public static void main(String[] args) {
//	EventQueue.invokeLater(new Runnable() {
//		public void run() {
//			try {
//				ViewGUI window = new ViewGUI();
//				//window.frame.setVisible(true);
//			} catch (Exception e) {
//	 			e.printStackTrace();
//			}
//		}
//	});}
public ViewGUI(User usr){
	this.user = usr;
	
JFrame frame = new JFrame();
frame.setTitle("Project Group B3");
Container cp = frame.getContentPane();
cp.setLayout(new FlowLayout());
cp.setBackground(Color.CYAN);

 cards = new CardLayout();
//*********************************Main Screen***************************

final JPanel Card3 = new JPanel();
Card3.setLayout(new FlowLayout());
final JPanel Box2 = new JPanel();
Box2.setLayout(new BoxLayout(Box2, BoxLayout.Y_AXIS));



Card3.setLayout(new GridLayout(3,2));
JLabel Mainlabel = new JLabel("View Requests ");
Mainlabel.setFont(new Font("Serif", Font.PLAIN, 28));
JLabel l0 = new JLabel("To view User favourite book list");
l0.setFont(new Font("Serif", Font.PLAIN, 20));
 butfav = new JButton ("Click Here for favourite books");
JLabel l01 = new JLabel("To Admin View Request of most Requested Books and Journals  ");
l01.setFont(new Font("Serif", Font.PLAIN, 20));
 butadmin = new JButton("Click Here to view requests ");


Box2.add(Mainlabel);
Box2.add(l0);
Box2.add(butfav);
Box2.add(l01);
Box2.add(butadmin);
Card3.add(Box2);



//--------------------------Favourites--------------------------------------------------------------

final JPanel Card4 = new JPanel();
 
Box Box0 = new Box(BoxLayout.Y_AXIS);
Box0.setBounds(0, 0, 515, 201);
Card4.setLayout(null);
Card4.setLayout(null);
JLabel Mlabel = new JLabel("List of the users Favoutire books");
Mlabel.setFont(new Font("Serif", Font.BOLD, 18));
JScrollPane favarea = new JScrollPane 
(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

Box0.add(Mlabel);
Card4.add(Box0);
Box0.add(favarea);

favarea0 = new JTextArea(6, 20);
favarea.setViewportView(favarea0); 


//--------------------------Admin request Views--------------------------------------------------------------
final JPanel Card5 = new JPanel();
Card5.setLayout(new FlowLayout());

Box adminview = new Box(BoxLayout.Y_AXIS);

Card5.setLayout(new GridLayout(6,1));
JLabel mainreqlb = new JLabel("Most requested Books and Journals");
mainreqlb.setFont(new Font("Serif", Font.BOLD, 18));

MreqB = new JButton("Get Most Requested Book");
bookreq = new JTextField();
MreqJ = new JButton("Get Most Requested Journals");
journalreq = new JTextField();

 butHomeG = new JButton("Back To main screen"); 

adminview.add(mainreqlb);


adminview.add(MreqB);
adminview.add(bookreq);
adminview.add(MreqJ);
adminview.add(journalreq);

adminview.add(butHomeG);
Card5.add(adminview);



//----------------------------------------------------------------

cardPanel = new JPanel();
cardPanel.setLayout(cards);

//----------------------------------------------------------------

cardPanel.add(Card3, "Panel2");
cardPanel.add(Card4, "Panel3");
 butHome = new JButton("Back To main screen");
butHome.setBounds(318, 212, 157, 23);
Card4.add(butHome);
submit = new JButton("Get List of favourites");
submit.setBounds(10, 212, 168, 23);
Card4.add(submit);
JLabel Fav0 = new JLabel("Favorite Book 1:");
Fav0.setBounds(20, 246, 93, 23);
Card4.add(Fav0);
JLabel fav1 = new JLabel("Favorite Book 2:");
fav1.setBounds(20, 269, 93, 23);
Card4.add(fav1);
JLabel fav2 = new JLabel("Favorite Book 3:");
fav2.setBounds(20, 292, 93, 23);
Card4.add(fav2);
JLabel fav3 = new JLabel("Favorite Book 4:");
fav3.setBounds(20, 314, 93, 23);
Card4.add(fav3);
JLabel fav4 = new JLabel("Favorite Book 5:");
fav4.setBounds(20, 337, 93, 23);
Card4.add(fav4);
fav0t = new JTextField();
fav0t.setBounds(123, 246, 127, 20);
Card4.add(fav0t);
fav0t.setColumns(10);
fav1t = new JTextField();
fav1t.setBounds(123, 270, 127, 20);
fav1t.setColumns(10);
Card4.add(fav1t);
fav2t = new JTextField();
fav2t.setBounds(123, 293, 127, 20);
fav2t.setColumns(10);
Card4.add(fav2t);
fav3t = new JTextField();
fav3t.setBounds(123, 315, 127, 20);
fav3t.setColumns(10);
Card4.add(fav3t);
fav4t = new JTextField();
fav4t.setBounds(123, 338, 127, 20);
fav4t.setColumns(10);
Card4.add(fav4t);
JLabel lblNewLabel = new JLabel("To add books to favorite type in the book title and click  update");
lblNewLabel.setBounds(10, 398, 355, 35);
lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 12));
Card4.add(lblNewLabel);
update = new JButton("Update Favorites");
update.setBounds(20, 371, 230, 23);
Card4.add(update);
JLabel result = new JLabel("Result =");
result.setBounds(260, 375, 46, 14);
Card4.add(result);
restf = new JTextField();
restf.setBounds(307, 372, 168, 20);
Card4.add(restf);
restf.setColumns(10);
clear = new JButton("clear all fields");
clear.setBounds(188, 212, 118, 23);
Card4.add(clear);
cardPanel.add(Card5, "Panel4");

//**********************************************************
clear.addActionListener(this);
submit.addActionListener(this);
MreqB.addActionListener(this); 
MreqJ.addActionListener(this);
update.addActionListener(this);
butadmin.addActionListener(this);
butfav.addActionListener(this);
butHomeG.addActionListener(this);
butHome.addActionListener(this);

cp.add(cardPanel);
frame.pack();
frame.setSize(600,500); 

frame.setVisible(true);
}

public void actionPerformed(ActionEvent e) // listens to buttons
{
	// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	ViewReq view = new ViewReq();
		
		int  usrId=  user.getId();			// Current user id

		Object target = e.getSource();
	if (target == MreqB) 
	{
		String Btitle = view.MostReqBook();
		bookreq.setText("The most Requested book is:  " + Btitle );
	        
	}
 	if (target == MreqJ)  
	{
 		String jtitle = view.MostReqJ();
		journalreq.setText("The most Requested Journal is:  " + jtitle ); 
 		
	}
	
 	if (target == submit) 
 		{ 			
 	 		String fav = view.userFav(usrId);
 	 		if (fav.length() >4){
 	 		favarea0.setText(fav);
 	 		}else {
 	 			restf.setText("No Favorites for this user");
 	 		}
 		}
 	if (target == clear) 
		{
 		fav0t.setText("");fav1t.setText("");fav2t.setText("");
	 		fav3t.setText("");fav4t.setText("");restf.setText("");favarea0.setText("");
		}
 	
 	if( target == update){
 
		
		String s1 = fav0t.getText();String s2 = fav1t.getText();
		String s3 = fav2t.getText();String s4 = fav3t.getText();
		String s5 = fav4t.getText();
		
		String res= view.addfav(usrId,s1,s2,s3,s4,s5);
			restf.setText(res);
 	}
 	if (target == butHome || target == butHomeG) {
 		
 		cards.show(cardPanel, "Panel2");
 		
 	}
 	if (target == butfav) {
 		cards.show(cardPanel, "Panel3");

 		
 	}
 	
 	if (target == butadmin) {
 		cards.show(cardPanel, "Panel4");
 		
 		
 	}
 	}//end ActionListenerMethod
}


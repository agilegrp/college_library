package Test;

import controller.SendGMail;
import junit.framework.TestCase;

public class SendGmailTest extends TestCase 
{
	SendGMail testObject = new SendGMail();
	
    //Test Number 1
	//Test Objective: Test all valid inputs
	//Input(s): sample message and valid email address
	//Expected Output: Mail sent successfully
	
	public void testSend_Mail0001()
	{
		String res = "Mail sent successfully";
		
		try 
		{	
	 	 	assertEquals(res, testObject.sendEmail("Test1","A00007055@student.ait.ie"));  	                                                    
		} 
		catch (Exception e) 
		{
			fail("Should not get here...No exception expected");      //indicator of unexpected failure case
		}
	}
	
	 	//Test Number 2
		//Test Objective: Test invalid inputs
		//Input(s): Malformed email address
		//Expected Output: Exception

		public void testSend_Mail0002()
		{		
			try 
			{
				testObject.sendEmail("Test1","A00007055:student.at.ie");  	      		
			} 
			catch (Exception e) 
			{
					assertSame("Malformed Email address",e.getMessage());  //assertEquals is Junit method		
			}
			
		}
	 	//Test Number 3
		//Test Objective: Test invalid inputs
		//Input(s): Null email address
		//Expected Output: Exception

		public void testSend_Mail0003()
		{		
			try 
			{
				testObject.sendEmail("Test1","");  	     		
			} 
			catch (Exception e) 
			{
				assertSame("Malformed Email address",e.getMessage());  //assertEquals is Junit method		
			}
			
	 	}	  	
		
		//Test Number 4
		//Test Objective: Test invalid inputs
		//Input(s): Null email address
		//Expected Output: Exception

		public void testSend_Mail0004()
		{
			try 
			{
		 		testObject.sendEmail("TestException","@");  	     		
			} 
			catch (Exception e) 
			{
				assertSame("Email failed exception",e.getMessage());  	
			}
			
	 	}
		
}

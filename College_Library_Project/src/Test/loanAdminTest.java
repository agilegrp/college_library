package Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import junit.framework.TestCase;
import controller.FineAdmin;
import controller.SendGMail;
import controller.loanAdmin;
import Doa.LibraryDao;

public class loanAdminTest extends TestCase {
	// Test Number 1
	// Test Objective: Test all valid inputs
	// Input(s): insert new loan
	// Expected true

	public void testnewLoan0001() {
		new LibraryDao()
				.updateDB("insert into user values(null,'Paul','paul','a00007055@student.ait.ie',123456)");
		new LibraryDao()
				.updateDB("insert into item values(null,'Java','coding','J Guru','Coders','software',1234567890,0,1)");

		Boolean result = true;
		try {
			loanAdmin testObject = new loanAdmin();

			assertEquals(result, testObject.insertNewLoan(
					"a00007055@student.ait.ie", "Java", 0, 0)); 
			
		} catch (Exception e) {
			fail("Should not get here...No exception expected"); 

		}
	}

	// Test Number 2
	// Test Objective: Test invalid email input
	// Input(s): insert wrong email id
	// Expected User not found

	public void testnewLoan0002() {
		try {
			loanAdmin testObject = new loanAdmin();

			testObject.insertNewLoan("pau@mail.com", "Java", 0, 0); 

		} catch (Exception e) {
			assertSame("User not found", e.getMessage()); 
		}
	}
	
	// Test Number 3
	// Test Objective: Test invalid email input
	// Input(s): insert wrong email id
	// Expected User not found

	public void testnewLoan0003() {
		try {
			loanAdmin testObject = new loanAdmin();

			testObject.insertNewLoan("a00007055@student.ait.ie", "Java", 0, 0); 

		} catch (Exception e) {
			assertSame("No copies left", e.getMessage()); 
		}
	}

	// Test Number 4
	// Test Objective: Test invalid email input
	// Input(s): insert wrong book title
	// Expected Item not found

	public void testnewLoan0004() {
		try {
			loanAdmin testObject = new loanAdmin();

			testObject.insertNewLoan("a00007055@student.ait.ie", "Javaz", 0, 0); 
																					
		} catch (Exception e) {
			assertSame("Item not found", e.getMessage()); 
		}
	}

	// Test Number 5
	// Test Objective: Test all valid inputs
	// Input(s): record loan return
	// Expected true

	public void testnewLoan0005() {
		Double result = 0.0;
		try {
			loanAdmin testObject = new loanAdmin();

			assertEquals(result,
					testObject.loanReturn("a00007055@student.ait.ie", "Java")); 
		} catch (Exception e) {
			fail("Should not get here...No exception expected"); 
		}
	}

	// Test Number 6
	// Test Objective: Test invalid email input
	// Input(s): insert wrong email id
	// Expected User not found

	public void testnewLoan0006() {
		try {
			loanAdmin testObject = new loanAdmin();

			testObject.loanReturn("pau@mail.com", "Java"); 
		} catch (Exception e) {
			assertSame("User not found", e.getMessage()); 
		}
 	}

	// Test Number 7
	// Test Objective: Test overdue return
	// Input(s): insert loandate 20 days old
	// Expected Fine returned

	public void testnewLoan0007() throws Exception {
		String fineDate = subDate(20);
		int usrId = 0;
		ResultSet rs = new LibraryDao()
				.queryDB("select user_id from user where email = 'a00007055@student.ait.ie'");
		while (rs.next()) {
			usrId = rs.getInt("USER_ID");
		}
		String cmd = "update loan set loandate = '" + fineDate
				+ "' where user_id = " + usrId + ";";

		new LibraryDao().updateDB(cmd);

		Double result = 5.7;
		try {
			loanAdmin testObject = new loanAdmin();

			assertEquals(result,testObject.loanReturn("a00007055@student.ait.ie", "Java"));

		} catch (Exception e) {
			fail("Should not get here...No exception expected");  
		}
	}
	
	// Test Number 8
	// Test Objective: Test invalid email input
	// Input(s): insert wrong email id
	// Expected User not found

	public void testnewLoan0008() {
		
		String result = addDate(10);
		
		try {
			loanAdmin testObject = new loanAdmin();

			assertEquals(result,testObject.addDate(10)); 
		} catch (Exception e) {
			fail("Should not get here...No exception expected"); 
		}
 	}

	// ==================================================================================
	public String subDate(int num) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone("GMT"));
		cal.add(Calendar.DATE, -num);
		String newdate = cal.get(Calendar.DATE) + "/"
				+ (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR);

		String[] parts = newdate.split("/");
		int dy = Integer.parseInt(parts[0]);
		int mn = Integer.parseInt(parts[1]);
		if (dy < 10)
			parts[0] = "0" + parts[0];
		if (mn < 10)
			parts[1] = "0" + parts[1];
		return parts[2] + "-" + parts[1] + "-" + parts[0];

	}
	
	public String addDate(int num)
	{
		Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));
          cal.add(Calendar.DATE, + num);
        String newdate = cal.get(Calendar.DATE) +"/" +
                (cal.get(Calendar.MONTH)+1) + "/" + cal.get(Calendar.YEAR);
        
        String[] parts = newdate.split("/");
         int dy = Integer.parseInt(parts[0]);
         int mn = Integer.parseInt(parts[1]);
        if (dy < 10) parts[0] = "0"+parts[0];
        if (mn < 10) parts[1] = "0"+parts[1];
        return parts[2]+"-"+parts[1]+"-"+parts[0];  
        
	}

} // end class


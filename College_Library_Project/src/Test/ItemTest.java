
    
 
package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import Doa.LibraryDao;
import controller.AddItem;
import controller.Register;
import junit.framework.TestCase;


public class ItemTest extends TestCase{
	AddItem test  =new AddItem();
	
	//Test Number:001;
	//Test Objective: Test connection to the database 
	//Input(s): connection = true
	//Expected Output(s)= connected
	
	public void testConnection(){
		
		try{
			LibraryDao add = new LibraryDao();
			assert(add.getConnection())!= null; 
		}
		catch(Exception e)
		{
			fail("Should not get here...No exception expected"); 
		}
		
		
	}
	
	
	    //Test Number:001;
		//Test Objective: Test for add id 
		//Input(s): id=1
		//Expected Output(s)= 1
		
	
	
	public void testID001() throws Exception{
		  
		

	
	try{
		AddItem testTarget = new AddItem();
		assertEquals(true,testTarget.saveItem(91, "f"," f", "f", "f", "f", 0, 0, 0));
	}catch(Exception e){
	fail("Should not get here...No exception expected");
	}
}
	
	 //Test Number:002;
	//Test Objective: Test for  id 
	//Input(s): id= -1
	//Expected Output(s)= add id is not successful
	
	public void testID002() throws Exception{
		try {
			AddItem test = new AddItem();
			test.saveItem(-1, "f"," f", "f", "f", "f", 0, 0, 0);
		}
		catch (Exception e)
		{
			assertEquals("invaild",e.getMessage());
		}
		
	}
	

    //Test Number:003;
	//Test Objective: Test for  id   
	//Input(s): id= 0
	//Expected Output(s)= 0

public void testID003() throws Exception{
	
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem( 90, "one"," f", "f", "f", "f", 0, 0, 0));
		
		}
		catch (Exception e)
		{
			fail("Should not get here...No exception expected"); 
			 
		}
	}



	    //Test Number:004;
		//Test Objective: Test for  title   
		//Input(s): title= one
		//Expected Output(s)= one
	
	public void testtitle004() throws Exception{
		
		
		try {
			AddItem test = new AddItem();
			assertEquals(true,test.saveItem(96, "one"," f", "f", "f", "f", 0, 0, 0));
			
			}
			catch (Exception e)
			{
				fail("Should not get here...No exception expected"); 
				 
			}
		}
	

	

    //Test Number:005;
	//Test Objective: Test for  title   
	//Input(s): title= empty
	//Expected Output(s)= add title is not successful

public void testtitle005() throws Exception{
	try {
		AddItem test = new AddItem();
		test.saveItem(1, ""," f", "f", "f", "f", 0, 0, 0);
	}
	catch (Exception e)
	{
		assertSame("invaild",e.getMessage());
	}
	
}
	
//Test Number:006
	//Test Objective: Test for title   
	//Input(s): title= !
	//Expected Output(s)= add title is  successful

public void testtitle006() throws Exception{
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(93, "!"," f", "f", "f", "f", 0, 0, 0));
	}
	catch (Exception e)
	{
		assertSame("add title is successful",e.getMessage());
	}
	
}

//Test Number:007;
	//Test Objective: Test for title   
	//Input(s): title= ONE
	//Expected Output(s)= add title is successful (one)

public void testtitle007() throws Exception{
	

	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(92, "ONE"," f", "f", "f", "f", 0, 0, 0));
		
		}
		catch (Exception e)
		{
			fail("Should not get here...No exception expected"); 
			 
		}
	}

	
//Test Number:008
	//Test Objective: Test for  subject 
	//Input(s): subject=computer
	//Expected Output(s)= add subject is  successful


	 
public void testSubject008() throws Exception{
	
	try {
		
		assertEquals(true,test.saveItem(100, "f"," computer", "f", "f", "f", 0, 0, 0));
	
		}
		catch (Exception e)
		{
			fail("Should not get here...No exception expected"); 
			 
		}
	}
    
    


//Test Number:009
	//Test Objective: Test for  subject 
	//Input(s): subject=COMPUTER
	//Expected Output(s)= add subject is  successful


	 
public void testSubject009() throws Exception{
	
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(99, "f"," COMPUTE", "f", "f", "f", 0, 0, 0));

		}
		catch (Exception e)
		{
			fail("Should not get here...No exception expected"); 
			 
		}
	}
 

//Test Number:0010
	//Test Objective: Test for  subject 
	//Input(s): subject=!
	//Expected Output(s)= add subject is  successful


	 
public void testSubject0010() throws Exception{
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(97, "f"," !", "f", "f", "f", 0, 0, 0));
	}
	catch (Exception e)
	{
		assertSame("add subject is not successful",e.getMessage());
	}
	
}


//Test Number:0011
	//Test Objective: Test for  subject 
	//Input(s): subject=empty
	//Expected Output(s)= add subject is  successful


	 
public void testSubject0011() throws Exception{
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(98, "f"," ", "f", "f", "f", 0, 0, 0));
	}
	catch (Exception e)
	{
		assertSame("add subject is  successful",e.getMessage());
	}
	
}



//Test Number:0012
	//Test Objective: Test for  author
	//Input(s): author= li 
	//Expected Output(s)= add author is  successful (li)


	 
public void testAuthor0012() throws Exception{
	
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(72, "f","f", "li", "f", "f", 0, 0, 0));
	
	
	}
	catch (Exception e)
	{
		fail("Should not get here...No exception expected"); 
		 
	}
}

//Test Number:0013
	//Test Objective: Test for author
	//Input(s): author= LI
	//Expected Output(s)= add author is  successful


	 
public void testAuthor0013() throws Exception{
	
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(71, "f","f", "LI", "f", "f", 0, 0, 0));
		
		}
		catch (Exception e)
		{
			fail("Should not get here...No exception expected"); 
			 
		}
	}

//Test Number:0014
	//Test Objective: Test for  author
	//Input(s): author= empty
	//Expected Output(s)= add author is not successful


	 
public void testAuthor0014() throws Exception{
	try {
		AddItem test = new AddItem();
		test.saveItem(1, "f","f", "", "f", "f", 0, 0, 0);
	}
	catch (Exception e)
	{
		assertSame("invaild",e.getMessage());
	}
	
}



//Test Number:0015
	//Test Objective: Test for  author
	//Input(s): author= ! 
	//Expected Output(s)= add author is  successful


	 
public void testAuthor0015() throws Exception{
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(70, "f","f", "!", "f", "f", 0, 0, 0));
	}
	catch (Exception e)
	{
		assertSame("add author is   successful",e.getMessage());
	}
	
}

//Test Number:0016
	//Test Objective: Test for add publisher
	//Input(s): publisher= liu 
	//Expected Output(s)= add publisher is  successful


	 
public void testPublisher0016() throws Exception{
	
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(83, "f","f", "f", "liu", "f", 0, 0, 0));

	}
	catch (Exception e)
	{
		fail("Should not get here...No exception expected"); 
		 
	}
}
//Test Number:0017
	//Test Objective: Test for  publisher
	//Input(s): publisher= LIU 
	//Expected Output(s)= add publisher is  successful


	 
public void testPublisher0017() throws Exception{

	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(82, "f","f", "f", "LIU", "f", 0, 0, 0));
	
	}
	catch (Exception e)
	{
		fail("Should not get here...No exception expected"); 
		 
	}
}




//Test Number:0018
	//Test Objective: Test for  publisher
	//Input(s): publisher= empty
	//Expected Output(s)= add publisher is not successful


	 
public void testPublisher0018() throws Exception{
	try {
		AddItem test = new AddItem();
		test.saveItem(1, "f","f", "f", "", "f", 0, 0, 0);
	}
	catch (Exception e)
	{
		assertSame("invaild",e.getMessage());
	}
	
}


//Test Number:0019
	//Test Objective: Test for  publisher
	//Input(s): publisher= !
	//Expected Output(s)= add publisher is successful


	 
public void testPublisher0019() throws Exception{
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(81, "f","f", "f", "", "f", 0, 0, 0));
	}
	catch (Exception e)
	{
		assertSame("add publisher is   successful",e.getMessage());
	}
	
}

//Test Number:0020
	//Test Objective: Test for  genre
	//Input(s): genre= software 
	//Expected Output(s)= add genre is  successful


	 
public void testGenre0020() throws Exception{
	
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(80, "f","f", "f", "f", "software", 0, 0, 0));
	
	}
	catch (Exception e)
	{
		fail("Should not get here...No exception expected"); 
		 
	}
}


//Test Number:0021
	//Test Objective: Test for  genre
	//Input(s): genre= SOFTWARE
	//Expected Output(s)= add genre is  successful


	 
public void testGenre0021() throws Exception{
	
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(79, "f","f", "f", "f", "SOFTWARE", 0, 0, 0));
		
	}
	catch (Exception e)
	{
		fail("Should not get here...No exception expected"); 
		 
	}
}

//Test Number:0022
	//Test Objective: Test for  genre
	//Input(s): genre= ! 
	//Expected Output(s)= add genre is   successful


	 
public void testGenre0022() throws Exception{
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(10, "f","f", "f", "f", "!", 0, 0, 0));
	}
	catch (Exception e)
	{
		assertSame("add genre is  successful",e.getMessage());
	}
	
}






//Test Number:0023
	//Test Objective: Test for  genre
	//Input(s): genre= empty 
	//Expected Output(s)= add genre is not successful


	 
public void testGenre0023() throws Exception{
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(76, "f","f", "f", "f", "", 0, 0, 0));
	}
	catch (Exception e)
	{
		assertSame("invaild",e.getMessage());
	}
	
}



//Test Number:0024
	//Test Objective: Test for  ISBN
	//Input(s): ISBN= 8 digits
	//Expected Output(s)= add journalsISBN is  successful


	 
public void testjournalsISBN0020() throws Exception{

	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(75, "f","f", "f", "f", "f", 12345678, 0, 0));
	
	}
	catch (Exception e)
	{
		fail("Should not get here...No exception expected"); 
		 
	}
}
//Test Number:0025
	//Test Objective: Test for  ISBN
	//Input(s): ISBN= 1234567
	//Expected Output(s)= add journalsISBN is not successful


	 
public void testjournalsISBN0025() throws Exception{
	try {
		AddItem test = new AddItem();
		test.saveItem(1, "f","f", "f", "f", "f",1234567, 0, 0);
	}
	catch (Exception e)
	{
		assertSame("invaild",e.getMessage());
	}
	
}

//Test Number:0026
	//Test Objective: Test for  ISBN
	//Input(s): ISBN= 1234567890
	//Expected Output(s)= add bookISBN is  successful


	 
public void testbookISBN0026() throws Exception{
	
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(89, "f","f", "f", "f", "f",1234567890, 0, 0));
		
	}
	catch (Exception e)
	{
		fail("Should not get here...No exception expected"); 
		 
	}
}

//Test Number:0027
	//Test Objective: Test for  ISBN
	//Input(s): ISBN= 123456
	//Expected Output(s)= add bookISBN is  NOT successful


	 
public void testbookISBN0027() throws Exception{
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(87, "f","f", "f", "f", "f",123456, 0, 0));
	}
	catch (Exception e)
	{
		assertSame("invaild",e.getMessage());
	}
	
}
//Test Number:0028
	//Test Objective: Test for  numloans
	//Input(s): numloans= 1
	//Expected Output(s)= add numloans is  successful


	 
public void testNumloans0028() throws Exception{

	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(86, "f","f", "f", "f", "f",0, 1, 0));
	
	}
	catch (Exception e)
	{
		fail("Should not get here...No exception expected"); 
		 
	}
}


//Test Number:0029
	//Test Objective: Test for add numloans
	//Input(s): numloans= 0
	//Expected Output(s)= add numloans is  successful


	 
public void testNumloans0029() throws Exception{
	
	
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(85, "f","f", "f", "f", "f",0, 0, 0));
	
	}
	catch (Exception e)
	{
		fail("Should not get here...No exception expected"); 
		 
	}
}
//Test Number:0030
	//Test Objective: Test for  numloans
	//Input(s): numloans= -1
	//Expected Output(s)= add numloans is not successful


	 
public void testNumloans0030() throws Exception{
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(84, "f","f", "f", "f", "f",0, -1, 0));
	}
	catch (Exception e)
	{
		assertSame("invaild",e.getMessage());
	}
	
}



//Test Number:0031
	//Test Objective: Test for stocknumber
	//Input(s): stocknumber=1
	//Expected Output(s)= add stocknumber is  successful


	 
public void teststocknumber0031() throws Exception{
	
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(74, "f","f", "f", "f", "f",1, 0, 1));
		
	}
	catch (Exception e)
	{
		fail("Should not get here...No exception expected"); 
		 
	}
}
//Test Number:0032
	//Test Objective: Test for  stocknumber
	//Input(s): stocknumber=0
	//Expected Output(s)= add stocknumber is  successful


	 
public void teststocknumber0032() throws Exception{

	
	try {
		AddItem test = new AddItem();
		assertEquals(true,test.saveItem(73, "f","f", "f", "f", "f",1, 0, 0));
		
	}
	catch (Exception e)
	{
		fail("Should not get here...No exception expected"); 
		 
	}
}

//Test Number:0033
	//Test Objective: Test for  stocknumber
	//Input(s): stocknumber=-1
	//Expected Output(s)= add stocknumber is not successful


	 
public void teststocknumber0033() throws Exception{
	try {
		AddItem test = new AddItem();
		test.saveItem(1, "f","f", "f", "f", "f",1, 0, -1);
	}
	catch (Exception e)
	{
		assertSame("invaild",e.getMessage());
	}
	
}
















}
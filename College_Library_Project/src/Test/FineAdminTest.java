package Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import junit.framework.TestCase;
import controller.FineAdmin;
import controller.SendGMail;
import Doa.LibraryDao;

public class FineAdminTest extends TestCase 
{
    //Test Number 1
	//Test Objective: Test all valid inputs
	//Input(s): date = today date - 7 days
	//Expected Output 7 Days
	
	public void testFine_CalcDays0001()
	{
		String tDate = subDate(7);

		try {
	 	 	FineAdmin testObject = new FineAdmin();
			
	 		assertEquals(6, testObject.calculateDays(tDate));  //assertEquals is Junit method
			                                                                   // 0.01 = delta, margin for recurring decimal place
		} 
		catch (Exception e) 
		{
	 		fail("Should not get here...No exception expected");      //indicator of unexpected failure case
		}
	}
	
	 	//Test Number 2
		//Test Objective: Test all valid inputs
		//Input(s): date = today date - 30 days
		//Expected Output 30 Days
		
		public void testFine_CalcDays0002()
		{
			String tDate = subDate(30);
			
			try 
			{
	 		 	FineAdmin testObject = new FineAdmin();
				
				assertEquals(29, testObject.calculateDays(tDate));  //assertEquals is Junit method
				                                                                   // 0.01 = delta, margin for recurring decimal place
			} 
			catch (Exception e) 
			{
				fail("Should not get here...No exception expected");      //indicator of unexpected failure case
			}
		}
		
	 	//Test Number 3
		//Test Objective: Test all valid inputs
		//Input(s): date = today date - 100 days
		//Expected Output 30 Days
		
		public void testFine_CalcDays0003()
		{
			String tDate = subDate(100);
			
			try 
			{
	 		 	FineAdmin testObject = new FineAdmin();
				
				assertEquals(99, testObject.calculateDays(tDate));  //assertEquals is Junit method
				                                                                   // 0.01 = delta, margin for recurring decimal place
			} 
			catch (Exception e) 
			{
				fail("Should not get here...No exception expected");      //indicator of unexpected failure case
			}
		}
			 	
		//Test Number 4
		//Test Objective: Test invalid inputs
		//Input(s): date = null
		//Expected Output Exception
		
		public void testFine_CalcDays0004()
		{
			try 
			{
	 			FineAdmin testObject = new FineAdmin();
				
				testObject.calculateDays("");  //assertEquals is Junit method
				                                                                   // 0.01 = delta, margin for recurring decimal place
			}  
			catch (Exception e) 
			{
				assertSame( "UnParseable data: \"\"",e.getMessage());
			}
		}
	 	//Test Number 5
		//Test Objective: Test invalid date format
		//Input(s): 
		//Expected Output: Exception Unparseable data
		
		public void testFine_CalcDays0005()
		{
			try 
			{
				FineAdmin testObject = new FineAdmin();
				
	 			testObject.calculateDays("03-01-2017");  //assertEquals is Junit method
				                                                                   // 0.01 = delta, margin for recurring decimal place
	 		} 
			catch (Exception e) 
			{
				assertSame( "UnParseable data: \"03-01-2017\"",e.getMessage());      //indicator of unexpected failure case
			}
	 	}
		
	 	//Test Number 6
		//Test Objective: Test all valid inputs
		//Input(s): date = today date - 0 days
		//Expected Output 0 Days
		
		public void testFine_CalcDays0006()
		{
			String tDate = subDate(0);
			
			try 
			{
	 			FineAdmin testObject = new FineAdmin();
				
				assertEquals(0, testObject.calculateDays(tDate));  //assertEquals is Junit method
				                                                                   // 0.01 = delta, margin for recurring decimal place
	 		} 
			catch (Exception e) 
			{
				fail("Should not get here...No exception expected");      //indicator of unexpected failure case
			}
	 	}

		
		//Test Number 7
		//Test Objective: Test invalid inputs
		//Input(s): Number of days = 0, Fine rate = 0
		//Expected Output Exception
		
		public void testFine_CalcFine0001()
		{
			FineAdmin testObject = new FineAdmin();
			new LibraryDao().updateDB("update admin set fineRate = '0'");
			try 
			{
				testObject.calculateFine(0);            
			}  
			catch (Exception e) 
			{ 
		 		assertSame( "A zero value was entered", e.getMessage());	 	
			}
	 	}
		
		//Test Number 8
		//Test Objective: Test invalid inputs
		//Input(s): Fine Rate = 0
		//Expected Output Exception
		
		public void testFine_CalcFine0002()
		{
			FineAdmin testObject = new FineAdmin();

			try 
			{				
				testObject.calculateFine(30);            
			}  
			catch (Exception e) 
			{
				assertSame( "A zero value was entered", e.getMessage());		 	
			}
		}
		
		//Test Number 9
		//Test Objective: Test valid inputs
		//Input(s): 30 days
		//Expected Output �1.50 fine
		
		public void testFine_CalcFine0003()
		{
			FineAdmin testObject = new FineAdmin();
			testObject.updateFineRate("admin1", "admin", 0.05);
			try  
			{	
	 			assertEquals(1.5, testObject.calculateFine(30), 0.01);  //assertEquals is Junit method	                                                                   // 0.01 = delta, margin for recurring decimal place
			}  
			catch (Exception e) 
			{
				fail("Should not get here...No exception expected");      //indicator of unexpected failure case
			}
		}
		
		//Test Number 10
		//Test Objective: Test invalid inputs
		//Input(s): Number of days = 0, Fine rate = 0
		//Expected Output Exception
		
		public void testFine_CalcFine0004()
		{
			FineAdmin testObject = new FineAdmin();
			new LibraryDao().updateDB("update admin set fineRate = '0'");
			try 
			{
				testObject.calculateFine(0);            
			}  
			catch (Exception e) 
			{ 
		 		assertSame( "A zero value was entered", e.getMessage());	 	
			}
	 	}
	
		
		//Test Number 11
		//Test Objective: Test valid inputs
		//Input(s): Fine Rate = 0.05
		//Expected Output Exception
		
		public void testFine_FineAdmin0001()
		{
			Boolean flag = true;
			try 
			{
			 	FineAdmin testObject = new FineAdmin();
				
	 	 		assertEquals(flag, testObject.updateFineRate("admin1","admin", 0.05));
			}  
			catch (Exception e) 
			{
	 			fail("Should not get here...No exception expected");	 		
			}
		}
		
		//Test Number 12
		//Test Objective: Test invalid inputs
		//Input(s): Fine Rate = 0
		//Expected Output Exception
		
		public void testFine_FineAdmin0002()
		{
			try 
			{
				FineAdmin testObject = new FineAdmin();
				
				testObject.updateFineRate("admin1","admin",0);            
			}  
			catch (Exception e) 
			{
	 	 		assertSame( "A zero value was entered", e.getMessage());		  	
			}
		}
		
		//Test Number 13
		//Test Objective: Test invalid inputs
		//Input(s): incorrect password
		//Expected Output Exception
		
		public void testFine_FineAdmin0003()
		{
			FineAdmin testObject = new FineAdmin();
			
			testObject.updateFineRate("admin1","admin",0.05);
			try 
			{
				
				
		 		testObject.updateFineRate("admin1","admn",0.05);            
			}  
			catch (Exception e) 
			{
	 			assertSame( "Incorrect password entered", e.getMessage());			
			}
		}
		
		//Test Number 14
		//Test Objective: View fine rate
		//Input(s): request fine rate
		//Expected Output fine rate
		
		public void testFine_FineAdmin0004()
		{
			try 
			{
				FineAdmin testObject = new FineAdmin();
				
				assertEquals("0.05", testObject.viewFineRate());           
			}  
			catch (Exception e) 
			{
	 			fail("Should not get here...No exception expected");
			}
		}
		
		//Test Number 15
		//Test Objective: View fine rate
		//Input(s): request fine rate
		//Expected Output fine rate
		
		public void testFine_FineAdmin0005()
		{
			try 
			{
				FineAdmin testObject = new FineAdmin();
				
				testObject.viewFineRate();           
			}  
		 	catch (Exception e) 
			{
				assertSame( "Sql Exception", e.getMessage());
			}
		}
		

		
	    //Test Number 16
		//Test Objective: Test all valid inputs
		//Input(s): sample message and valid email address
		//Expected Output: Mail sent successfully
		
		public void testSend_Mail0001()
		{
			String res = "Mail sent successfully";
			SendGMail testObject = new SendGMail();
			
			try {
				
				
				assertEquals(res, testObject.sendEmail("Mail Test1","A00007055@student.ait.ie"));  
				                                                    
			} 
			catch (Exception e) 
			{
				fail("Should not get here...No exception expected");      //indicator of unexpected failure case
			}
		}
		
		 	//Test Number 17
			//Test Objective: Test invalid inputs
			//Input(s): Malformed email address
			//Expected Output: Exception

			public void testSend_Mail0002()
			{
				
				SendGMail testObject = new SendGMail();
			
			try 
			{
				testObject.sendEmail("Mail Test2","A00007055:student.at.ie");  	     		
			} 
			catch (Exception e) 
			{
					assertSame("Malformed Email address",e.getMessage());  //assertEquals is Junit method		
			}
				
			}
		 	//Test Number 18
			//Test Objective: Test invalid inputs
			//Input(s): Null email address
			//Expected Output: Exception

			public void testSend_Mail0003()
			{
				
				SendGMail testObject = new SendGMail();
			
			try 
			{
				testObject.sendEmail("Mails Test3","");  	     		
			} 
			catch (Exception e) 
			{
					assertSame("Malformed Email address",e.getMessage());  //assertEquals is Junit method		
			}
				
		 	}
		
		 	//Test Number 19
			//Test Objective: Test valid inputs
			//Input(s): set users book fine amount
			//Expected Output: fine updated

			public void testSetFine0001()
			{
				FineAdmin testObject = new FineAdmin();
				String res = "Fine updated";
			
				try 
				{
					assertEquals(res, testObject.setUserFine(1.5, "1","A00007055@student.ait.ie"));  	     		
				} 
				catch (Exception e) 
				{
					fail("Should not get here...No exception expected");  //assertEquals is Junit method		
				}
				
		 	}
		 	//Test Number 20
			//Test Objective: Test valid inputs
			//Input(s): set users journal fine amount
			//Expected Output: fine updated

			public void testSetFine0002()
			{
				FineAdmin testObject = new FineAdmin();
				String res = "Fine updated";
			
				try 
				{
					assertEquals(res, testObject.setUserFine(1.5, "1","A00007055@student.ait.ie"));  	     		
				} 
	 			catch (Exception e) 
				{
			 		fail("Should not get here...No exception expected");  //assertEquals is Junit method		
				}
				
		 	}
		 	//Test Number 21
			//Test Objective: Test valid inputs
			//Input(s): check active loans
			//Expected Output: Fine Updated

			public void testcheckFines0001()
			{
				FineAdmin testObject = new FineAdmin();
				String tDate = subDate(33);
				new LibraryDao().updateDB("update loan set loandate = '"+tDate+"' where loan_id = 0");
				String res = "Fines Pending\na00007055@student.ait.ie\n";
				
				try 
				{
					assertEquals(res, testObject.checkFines());  
				} 
				catch (Exception e) 
				{
					fail("Should not get here...No exception expected"); 
				}
				
		 	}
		 	//Test Number 22
			//Test Objective: Test valid inputs
			//Input(s): set users journal fine amount
			//Expected Output: No updates performed

			public void testcheckFines0002()
			{
				FineAdmin testObject = new FineAdmin();
				String tDate = subDate(5);
				new LibraryDao().updateDB("update loan set loandate = '"+tDate+"' where loan_id = 0");
				String res = "No Fines Pending";
				              
				try 
				{
					assertEquals(res, testObject.checkFines());  	     		
				} 
	 			catch (Exception e) 
				{
			 		fail("Should not get here...No exception expected");  //assertEquals is Junit method		
				}
				
		 	}
			
		 	//Test Number 23
			//Test Objective: Test valid inputs
			//Input(s): check if a user has a fine pending
			//Expected Output: No fines pending

			public void testcheckFines0003()
			{
				new LibraryDao().updateDB("delete from loan  where loan_id =0");
				new LibraryDao().updateDB("delete from loan  where loan_id =1");
				new LibraryDao().updateDB("INSERT INTO LOAN VALUES(0,1,'2017-02-28','2017-02-21','2017-02-21','1.00','0001')");
				new LibraryDao().updateDB("INSERT INTO LOAN VALUES(1,2,'2017-02-28','2017-02-21','2017-02-21','0.00','0001')");

				FineAdmin testObject = new FineAdmin();
				String res = "No Fines Pending";
			
				try 
				{
					assertEquals(res, testObject.checkMyLoans(2));  	     		
				} 
	 			catch (Exception e) 
				{
			 		fail("Should not get here...No exception expected");  //assertEquals is Junit method		
				}
				
		 	}
			
		 	//Test Number 24
			//Test Objective: Test valid inputs
			//Input(s): check if a user has a fine pending
			//Expected Output: Fines pending

			public void testcheckFines0004()
			{
				FineAdmin testObject = new FineAdmin();
				String res = "Fines pending";
			
				try 
				{
					assertEquals(res, testObject.checkMyLoans(1));  	     		
				} 
	 			catch (Exception e) 
				{
			 		fail("Should not get here...No exception expected");  //assertEquals is Junit method		
				}
				
		 	}

//==================================================================================	
		public String subDate(int num)
		{
			Calendar cal = Calendar.getInstance();
	        cal.setTimeZone(TimeZone.getTimeZone("GMT"));
	          cal.add(Calendar.DATE, - num);
	        String newdate = cal.get(Calendar.DATE) +"/" +
	                (cal.get(Calendar.MONTH)+1) + "/" + cal.get(Calendar.YEAR);
	        
	        String[] parts = newdate.split("/");
	         int dy = Integer.parseInt(parts[0]);
	         int mn = Integer.parseInt(parts[1]);
	        if (dy < 10) parts[0] = "0"+parts[0];
	        if (mn < 10) parts[1] = "0"+parts[1];
	        return parts[2]+"-"+parts[1]+"-"+parts[0];  
	        
		}
		

} //end class


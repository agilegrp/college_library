package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import controller.CancelReq;

public class CancelReqTest {

	// Test Number: 001
	// Test Objective: To test for ref cancel
	// Input(s): reference = 112222 
	// Expected Output(s) = invalid reference
	@Test
	public void testgetref001() throws Exception {
		try{
		CancelReq test = new CancelReq();
		test.getref("112222");
		}catch (Exception e) 
		{
			assertSame("Invaild Refernce", e.getMessage());		 	
		}
	}
	// Test Number: 002
	// Test Objective: To test for Ref cancel
	// Input(s): Ref = 4
	// Expected Output(s) = Req Canceled 	
	@Test
	public void testgetref002() throws Exception {
		String result= "1";

	try{
		CancelReq test = new CancelReq();
		test.getref("1");
		assertEquals("1", result); 

	}catch (Exception e){
		fail("Should not get here...No exception expected"); 
		}
	
	}
	// Test Number: 003
		// Test Objective: To test for ref cancel
		// Input(s): reference = null 
		// Expected Output(s) = invalid reference
		@Test
		public void testgetref003() throws Exception {
			try{
			CancelReq test = new CancelReq();
			test.getref("-1");
			}catch (Exception e) 
			{
				assertSame("Invaild Refernce", e.getMessage());		 	
			}
		}
}

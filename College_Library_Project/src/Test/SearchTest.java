package Test;

import static org.junit.Assert.*;
import org.junit.Test;

import Doa.LibraryDao;
import controller.SearchItem;

public class SearchTest {
	
	SearchItem testTarget = new SearchItem();
	@Test	
	//Test Number: 1
	//Test Objective: Test valid book title input
	//Input(s): Book Title = one 
	//Expected Output: book details returned.
	
	public void testSearchBookTitle0001(){
	
		try{
			//SearchItem testTarget = new SearchItem();
			testTarget.searchBookTitle("one");
		}
		catch(Exception e)
		{
			fail("Should not get here...No exception expected");
		}
	}

	
	//Test Number: 2
	//Test Objective: Test invalid book title input
	//Input(s): Book Title = one! 
	//Expected Output: Exception Message "Invalid Search; This book does not exist in the library"
	@Test
	public void testSearchBookTitle0002(){
				
		try{
			testTarget.searchBookTitle("one!");
		}catch (Exception e)
		{
			assertSame("Invalid Search; This book does not exist in the library.", e.getMessage());
		}
	}
	
	//Test Number: 3
	//Test Objective: Test valid journal title input
	//Input(s): Journal Title = one 
	//Expected Output: journal details returned.
		@Test
		public void testSearchJournalTitle0003(){
						
			try{
				testTarget.searchJournalTitle("one"); 
			}
			catch(Exception e) 
			{
				fail("Should not get here...No exception expected");
			}
		}
		
		//Test Number: 4
		//Test Objective: Test invalid journal title input
		//Input(s): journal Title = one! 
		//Expected Output: Exception Message "Invalid Search; This journal does not exist in the library."
		@Test
		public void testSearchJournalTitle0004(){
					
			try{
				testTarget.searchJournalTitle("one!");
			}catch (Exception e)
			{
				assertSame("Invalid Search; This journal does not exist in the library.", e.getMessage());
			}
		}
		
		//Test Number: 5
		//Test Objective: Test valid book author input
		//Input(s): Book author = authorone  
		//Expected Output: Book Author Found.
		@Test
		public void testSearchBookAuthor0005(){
			
			try{
				testTarget.searchBookAuthor("authone"); 
			}
			catch(Exception e) 
			{
				fail("Should not get here...No exception expected");
			}
		}
		
		//Test Number: 6
		//Test Objective: Test invalid book author input
		//Input(s): Book author = authorone! 
		//Expected Output: Exception
		@Test
		public void testSearchBookAuthor0006(){
					
			try{
				testTarget.searchBookAuthor("authone!");
			}catch (Exception e)
			{
				assertSame("Invalid Search; This book author does not exist in the library.", e.getMessage());
			}
		}
		
		//Test Number: 7
		//Test Objective: Test valid journal author input
		//Input(s): journal title = authorone
		//Expected Output: journal details
		@Test
		public void testSearchJournalAuthor0007(){
		try{
			testTarget.searchJournalAuthor("authone");
			}
		catch(Exception e)
			{
				fail("Should not get here...No exception expected");
			}
		}
		
		@Test	
		//Test Number: 8
		//Test Objective: Test invalid journal author input
		//Input(s): journal author = authorone! 
		//Expected Output: exception
		
		public void testSearchJournalAuthor0008(){
		
			try{
				
				testTarget.searchJournalAuthor("authone!");
			}
			catch(Exception e)
			{
				assertSame("Invalid Search; This journal author does not exist in the library.", e.getMessage());
			}
		}

		
		//Test Number: 9
		//Test Objective: Test valid book reference number (id number) input
		//Input(s): Book id = 0
		//Expected Output: book details
			@Test
			public void testsearchBookRefNo0009(){
			try{
				testTarget.searchBookRefNo("0");
				}
			catch(Exception e)
				{
					fail("Should not get here...No exception expected");
				}
			}
			
			//Test Number: 10
			//Test Objective: Test invalid book reference number (id number) input
			//Input(s): Book id = " "
			//Expected Output: exception
				@Test
				public void testsearchBookRefNo0010(){
				try{
					testTarget.searchBookRefNo(" ");
					}
				catch(Exception e)
				{
					assertSame("Invalid Search; This book refrence no does not exist in the library.", e.getMessage());
				}
			}
				
				//Test Number: 11
				//Test Objective: Test valid journal reference number (id number) input
				//Input(s): Journal id = 0
				//Expected Output: journal details
					@Test
					public void testSearchJournalRefNo0009(){
					try{
						testTarget.searchJournalRefNo("0");
						}
					catch(Exception e)
						{
							fail("Should not get here...No exception expected");
						}
					} 
					
					//Test Number: 12
					//Test Objective: Test invalid book reference number (id number) input
					//Input(s): Book id = " "
					//Expected Output: exception
						@Test
						public void testsearchJournalRefNo0012(){
						try{
							testTarget.searchBookRefNo(" ");
							}
						catch(Exception e)
						{
							assertSame("Invalid Search; This book refrence no does not exist in the library.", e.getMessage());
						}
					}
						
						
						//Test Number: 13
						//Test Objective: Test valid book ISBN
						//Input(s): Book id = 0
						//Expected Output: book details
							@Test
							public void testsearchISBN0013(){
							try{
								testTarget.searchBookISBN("1234567890");
								}
							catch(Exception e)
								{
									fail("Should not get here...No exception expected");
								}
							}
							
							//Test Number: 14
							//Test Objective: Test invalid book ISBN input
							//Input(s): Book id = " "
							//Expected Output: exception
								@Test
								public void testsearchBookRefNo0014(){
								try{
									testTarget.searchBookISBN("123456789");
									}
								catch(Exception e)
								{
									assertSame("Invalid Search; ISBN must be 10 digits long.", e.getMessage());
								}
							}				
						
						
					
				// Test Number: 0000
				// Test Objective: Test connection to the database
				// Input(s): connection= true 
				// Expected Output(s) = connected
		 @Test
		 public void testDatabaseConnection() {
		       try{
		        LibraryDao lbDView = new LibraryDao();
		        assert (lbDView.getConnection()) != null;
		      	}catch (Exception e) 
		    	{
		     		fail("Should not get here...No exception expected");      //indicator of unexpected failure case
		    	}
		      	}
		 
		 
		
		 
		 
		 //========================Sprint Two=========================
		 
		//Test Number: 15
			//Test Objective: Test to retrieve the number of copies of a book.
			//Input(s): Book id = 0
			//Expected Output: book details including the number of copies
				@Test
				public void testStockNumberN0015(){
					
						
					}		
		 
}

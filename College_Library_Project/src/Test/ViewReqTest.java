package Test;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Assert;
import org.junit.Test;

import com.sun.javafx.fxml.expression.Expression;

import Doa.LibraryDao;
import controller.PlaceReq;
import controller.ViewReq;
import junit.framework.TestCase;

public class ViewReqTest extends TestCase  {

	 ViewReq testrget = new ViewReq();
	 ResultSet rs;
	 
	 
	 		// Test Number: 001
			// Test Objective: Test connection to the database
			// Input(s): connection= true 
			// Expected Output(s) = connected
	 @Test
	 public void testConnection() {
	       
		 try{
	        LibraryDao lbDView = new LibraryDao();
	        assert (lbDView.getConnection()) != null;
	      	}catch (Exception e) 
	    	{
	     		fail("Should not get here...No exception expected");      //indicator of unexpected failure case
	    	}
	      	}
	
		// Test Number: 0002
		// Test Objective: To test for most requested book 
		// Input(s): string query
		// Expected Output(s) = Book

	public void testSqlbookreq0000(){
		ViewReq testTarget = new ViewReq();
	try{
		String actual0 = testTarget.MostReqBook();
		String res = "Book";
		
		
		assertEquals(res,actual0 );
	}catch (Exception e) 
	{
		fail("Should not get here...No exception expected");      //indicator of unexpected failure case
	}
	}
		// Test Number: 003
		// Test Objective: To test for most requested book 
		// Input(s): string query
		// Expected Output(s) = one

	public void testSqlbookreq0001(){
		ViewReq testTarget = new ViewReq();
  	try{
  		String expectedResult = "No Books yet Requested";
  		
  		assertEquals(expectedResult, testTarget.MostReqBook());
	}catch (Exception e) 
	{
 		fail("Should not get here...No exception expected");      //indicator of unexpected failure case
	}
  	}
//	
//	// Test Number: 001
//			// Test Objective: To test for most requested book 
//			// Input(s): string query
//			// Expected Output(s) = one
//
//		public void testSqlbookreq003(){
//			ViewReq testTarget = new ViewReq();
//	  	try{
//	  		boolean input = true;
//	  		boolean expectedResult = false;
//	  		
//	  		assertEquals( expectedResult,testTarget.MostReqBook().equals(input));
//		}catch (Exception e) 
//		{
//	 		fail("Should not get here...No exception expected");      //indicator of unexpected failure case
//		}
//	  	}
//		
//	
//	// Test Number: 004
//			// Test Objective: To test for most requested book fails
//			// Input(s): string query
//			// Expected Output(s) = one
//
//		public void testSqlbookreq0004(){
//			ViewReq testTarget = new ViewReq();
//
//	  	try{
//		 String Result = "Book";
//		
//		 //assertNotEquals(NotResult, testTarget.MostReqBook());
//		 assertEquals(testTarget.MostReqBook(), Result);
//	  	}catch (Exception e) 
//		{
//	 		fail("Should not get here...No exception expected");      //indicator of unexpected failure case
//		}
//	  	}
//	
	// Test Number: 002
	// Test Objective: To test for most requested journal 
	// Input(s): string query
	// Expected Output(s) = 

	public void testJournalreq0001(){
		ViewReq testTarget = new ViewReq();
  	try{
	 String actual = "No Journals yet Requested";
	  String actual0 = testTarget.MostReqJ();
	 
	 assertEquals(actual0, testTarget.MostReqJ());
  	}catch (Exception e) 
	{
 		fail("Should not get here...No exception expected");      //indicator of unexpected failure case
	}
  	}
//	
	// Test Number: 002
	// Test Objective: To test for most requested journal not a string
	// Input(s): string query
	// Expected Output(s) = 

	public void testJournalreq0002(){
		ViewReq testTarget = new ViewReq();
  	try{

	 String res = "Journal1";
	 assertEquals( res,testTarget.MostReqJ());
  	}catch (Exception e) 
	{
 		fail("Should not get here...No exception expected");      //indicator of unexpected failure case
	}
  	}

	
	
	
	// Test Number: 003
	// Test Objective: To test list of favourite books
	// Input(s): string query
	// Expected Output(s) = 
	public void testUserFavorite(){
		ViewReq testTarget = new ViewReq();
		
	 String expectedResult = "No favorites yet";
	 
	 assertEquals(expectedResult, testTarget.userFav(2));
	}
	// Test Number: 003
	// Test Objective: To test list of favourite books
	// Input(s): string query
	// Expected Output(s) = 
	public void testUserFavorite003(){
		ViewReq testTarget = new ViewReq();
		
	 String expectedResult = "Your logged in as Admin";
	 
	 assertEquals(expectedResult, testTarget.userFav(0));
	}
	
	
	// Test Number: 004
		// Test Objective: To test list of favourite books
		// Input(s): string query for user id 2
		// Expected Output(s) = no favourites
		public void testUserFavorite02(){
			ViewReq testTarget = new ViewReq();
			
		 String expectedResult = "book1\nbook2\nBook3\n\n";
		 
		 assertEquals(expectedResult, testTarget.userFav(1));
		}

		// Test Number: 0001
		// Test Objective: To test adding favourites
		// Input(s): string query for user id 
		// Expected Output(s) = table updated 
		public void testaddfav0001(){
			ViewReq testTarget = new ViewReq();
			
		 String expectedResult = "table updated";
		 
		 assertEquals(expectedResult, testTarget.addfav(1,"1","2","3","",""));
		}
		// Test Number: 0002
		// Test Objective: To test adding favourites
		// Input(s): string query for user id 
		// Expected Output(s) = table created
		public void testaddfav0002(){
			ViewReq testTarget = new ViewReq();
			
		 String expectedResult = "table created";
		 
		 assertEquals(expectedResult, testTarget.addfav(20,"","","","",""));
		}
		
		// Test Number: 0003
		// Test Objective: To test adding favourites
		// Input(s): string query for user id 
		// Expected Output(s) = Your logged in as Admin
		public void testaddfav0003(){
			ViewReq testTarget = new ViewReq();
			
		 String expectedResult = "Your logged in as Admin";
		 
		 assertEquals(expectedResult, testTarget.addfav(0,"1","2","3","",""));
		}

	 


}


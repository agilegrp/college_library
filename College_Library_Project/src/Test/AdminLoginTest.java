package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import controller.AdminLogin;

public class AdminLoginTest {

	// Test Number: 001
		// Test Objective: verify username and password done
		// Input(s): Username = Admin
	  // Input(s): Password = 123
		// Expected Output(s) = invalid details
		@Test
		public void testlogin001() throws Exception {
			try{
			AdminLogin test = new AdminLogin();
			test.login("Admin1","124");
			}catch (Exception e) 
			{
				assertSame("invalid details", e.getMessage());		 	
			}
		}
		
		// Test Number: 002
		// Test Objective: To test for title 
		// Input(s): user admin password 123 
		// Expected Output(s) = Admin 123
			@Test
			public void testlogin002() throws Exception {
				String result= "admin1 123";
				//String result1= "123";
				try{
					AdminLogin test = new AdminLogin();
					test.login("ADMIN1","123");
					assertEquals("admin1 123", result); 
				} 
				catch (Exception e) 
				{
				fail("Should not get here...No exception expected"); 

				}
			}
			
			// Test Number: 003
			// Test Objective: verify username and password
			// Input(s): Username = aa
		  // Input(s): Password = 123
			// Expected Output(s) = invalid details
			@Test
			public void testlogin003() throws Exception {
				try{
				AdminLogin test = new AdminLogin();
				test.login("Aa","123");
				}catch (Exception e) 
				{
					assertSame("invalid details", e.getMessage());		 	
				}
			}
			// Test Number: 004
			// Test Objective: verify username and password
			// Input(s): Username = aa 
			// Input(s): Password = 111
			// Expected Output(s) = invalid details
			@Test
			public void testlogin004() throws Exception {
				try{
				AdminLogin test = new AdminLogin();
				test.login("aa","111");
				}catch (Exception e) 
				{
					assertSame("invalid details", e.getMessage());		 	
				}
			}
}

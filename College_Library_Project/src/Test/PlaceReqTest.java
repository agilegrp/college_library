package Test;


import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import controller.PlaceReq;


public class PlaceReqTest extends PlaceReq{
	
	

	// Test Number: 001
	// Test Objective: To test for title 
	// Input(s): title = one 
	// Expected Output(s) = ITEM IS AVAILABLE
	@Test
	public void testgettitle001() throws Exception {
		try{
		PlaceReq test = new PlaceReq();
		test.gettitle("one",1,0,0);
		}catch (Exception e) 
		{
			assertSame("ITEM IS AVAILABLE", e.getMessage());		 	
		}
	}
		// Test Number: 002
		// Test Objective: To test for title 
		// Input(s): title = ONE 
		// Expected Output(s) = ITEM IS AVAILABLE
	@Test
	public void testgettitle002() throws Exception {
		try{
		PlaceReq test = new PlaceReq();
		test.gettitle("ONE",1,0,0);
		}catch (Exception e) 
		{
			assertSame("ITEM IS AVAILABLE", e.getMessage());		 	
		}
	}
		// Test Number: 003
		// Test Objective: To test for title 
		// Input(s): title = two 
		// Expected Output(s) = TITLE NOT FOUND	
	@Test
	public void testgettitle003() throws Exception {
		try{
			PlaceReq test = new PlaceReq();
			test.gettitle("two",1,0,0);
		}catch (Exception e){
		assertSame("TITLE NOT FOUND", e.getMessage());
		}
		
	}
	
	// Test Number: 004
	// Test Objective: To test for title 
	// Input(s): title = BookTitle 
	// Expected Output(s) = 0
		@Test
		public void testgettitle005() throws Exception {
			String result= "0";
			try{
				PlaceReq test = new PlaceReq();
				test.gettitle("Title",1,0,0);
				assertEquals("0", result); 
			} 
			catch (Exception e) 
			{
			fail("Should not get here...No exception expected"); 

			}
		}
		// Test Number: 005
		// Test Objective: To test for title 
		// Input(s): title = title 
		// Expected Output(s) = 0	
			@Test
			public void testgettitle004() throws Exception {
				String result= "0";
				try{
					
					PlaceReq test = new PlaceReq();
					test.gettitle("title",1,0,0);
					assertEquals("0", result); 
				} 
				catch (Exception e) 
				{
				fail("Should not get here...No exception expected"); 

				}
			}
	// Test Number: 001
	// Test Objective: To test for isbn 
	// Input(s): isbn =  1234567890
	// Expected Output(s) = ISBN NOT FOUND	
	@Test
	public void testgetisbn001() {
		try{
			PlaceReq test = new PlaceReq();
			test.getisbn("1234567890",1,0,0);
		}
		catch (Exception e){
			assertSame("ITEM IS AVAILABLE", e.getMessage());
			}
	}
	// Test Number: 002
	// Test Objective: To test for isbn 
	// Input(s): isbn =  12345678901
	// Expected Output(s) = ISBN must be 10 digit
	@Test
	public void testgetisbn002() {

		try{
			PlaceReq test = new PlaceReq();
			test.getisbn("12345678901",1,0,0);
		}catch (Exception e){
		assertSame("ISBN must be 10 digit", e.getMessage());
		}
		
	}
	// Test Number: 003
	// Test Objective: To test for isbn 
	// Input(s): isbn =  123456789
	// Expected Output(s) = ISBN must be 10 digit
	@Test
	public void testgetisbn003() {

		try{
			PlaceReq test = new PlaceReq();
			test.getisbn("123456789",1,0,0);
		}catch (Exception e){
			assertSame("ISBN must be 10 digit", e.getMessage());
		}
		
	}

	// Test Number: 004
	// Test Objective: To test for isbn 
	// Input(s): isbn =  1234567899
	// Expected Output(s) = ISBN NOT FOUND
	
	@Test
	public void testgetisbn004() {

		try{
			PlaceReq test = new PlaceReq();
			test.getisbn("1234567899",1,0,0);
		}catch (Exception e){
		assertSame("ISBN NOT FOUND", e.getMessage());
		}
	}
		// Test Number: 006
		// Test Objective: To test for isbn 
		// Input(s): isbn =  1122334455
		// Expected Output(s) = 0
		
		@Test
		public void testgetisbn006() {
			String result="0";
			try{
				PlaceReq test = new PlaceReq();
				test.getisbn("1122334455",1,0,0);
				assertEquals("0", result);
			} 
			catch (Exception e) 
			{
			fail("Should not get here...No exception expected"); 

			}
		}

		
}

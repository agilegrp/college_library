package Test;

import static org.junit.Assert.*;	

import org.junit.Test;

import controller.AdminLogin;
import controller.Register;
import controller.SendGMail;
import controller.UserLogin;
import controller.ViewReq;
import junit.framework.TestCase;
import model.User;

public class UserLoginTest extends TestCase {
	// Test Number: 001
	// Test Objective: Test insert the  valid register information in the database
	// Input(s): "11","11","11","11"
	// Expected Output(s):true
	@Test
	public void testRegister001() {
		try{
			Register testTarget = new Register();
			assertEquals(true, testTarget.saveUser("11","11","11","1111111111"));
		}catch(Exception e){
		fail("Should not get here...No exception expected");
		}
	}
	// Test Number: 002

		// Test Objective: Test register with valid user_name,
	                 //valid password,valid email,invalid phoneNumber
		// Input(s): "11","11","11","11111111111"
		// Expected Output(s):invalid register details
	public void testRegister002() {
		try{
			Register testTarget = new Register();
			testTarget.saveUser("11","11","11","11111111111");
		}catch(Exception e){
			assertSame("invalid register details", e.getMessage());
		}
	}
	
	// Test Number: 003

			// Test Objective: Test register with valid user_name,
		                 //valid password,invalid email,valid phoneNumber
			// Input(s): "11","11","1111111111111111111111","11"
			// Expected Output(s):invalid register details
	public void testRegister003() {
		try{
			Register testTarget = new Register();
			testTarget.saveUser("11","11","1111111111111111111111","1111111111");
		}catch(Exception e){
			assertSame("invalid register details", e.getMessage());
		}
	}
	// Test Number: 004

				// Test Objective: Test register with valid user_name,
			                 //valid password,invalid email,invalid phoneNumber
				// Input(s): "11","11","1111111111111111111111","11111111111"
				// Expected Output(s):invalid register details
	public void testRegister004() {
		try{
			Register testTarget = new Register();
			testTarget.saveUser("11","11","1111111111111111111111","11111111111");
		}catch(Exception e){
			assertSame("invalid register details", e.getMessage());
		}
	}
	// Test Number: 005

	// Test Objective: Test register with valid user_name,
                 //invalid password,valid email,valid phoneNumber
	// Input(s): "11","1111111111111111111111","11","11"
	// Expected Output(s):invalid register details
	public void testRegister005() {
		try{
			Register testTarget = new Register();
			testTarget.saveUser("11","1111111111111111111111","11","1111111111");
		}catch(Exception e){
			assertSame("invalid register details", e.getMessage());
		}
	}
	// Test Number: 006

		// Test Objective: Test register with valid user_name,
	                 //invalid password,valid email,invalid phoneNumber
		// Input(s): "11","1111111111111111111111","11","11111111111"
		// Expected Output(s):invalid register details
	public void testRegister006() {
		try{
			Register testTarget = new Register();
			testTarget.saveUser("11","1111111111111111111111","11","11111111111");
		}catch(Exception e){
			assertSame("invalid register details", e.getMessage());
		}
	}
	// Test Number: 007

			// Test Objective: Test register with valid user_name,
		                 //invalid password,invalid email,valid phoneNumber
			// Input(s): "11","1111111111111111111111","1111111111111111111111","1111111111"
			// Expected Output(s):invalid register details
	public void testRegister007() {
		try{
			Register testTarget = new Register();
			testTarget.saveUser("11","1111111111111111111111","1111111111111111111111","1111111111");
		}catch(Exception e){
			assertSame("invalid register details", e.getMessage());
		}
	}
	// Test Number: 008

				// Test Objective: Test register with valid user_name,
			                 //invalid password,invalid email,invalid phoneNumber
				// Input(s): "11","1111111111111111111111","1111111111111111111111","11111111111"
				// Expected Output(s):invalid register details
	public void testRegister008() {
		try{
			Register testTarget = new Register();
			testTarget.saveUser("11","1111111111111111111111","1111111111111111111111","11111111111");
		}catch(Exception e){
			assertSame("invalid register details", e.getMessage());
		}
	}
	// Test Number: 009

	// Test Objective: Test register with invalid user_name,
                 //invalid password,invalid email,invalid phoneNumber
	// Input(s): "1111111111111111111111","1111111111111111111111","1111111111111111111111","11111111111"
	// Expected Output(s):invalid register details
	public void testRegister009() {
		try{
			Register testTarget = new Register();
			testTarget.saveUser("1111111111111111111111","1111111111111111111111","1111111111111111111111","11111111111");
		}catch(Exception e){
			assertSame("invalid register details", e.getMessage());
		}
	}
	// Test Number: 010

	// Test Objective: Test register with invalid user_name,
                 //valid password,valid email,invalid phoneNumber
	// Input(s): "1111111111111111111111","11","11","11111111111"
	// Expected Output(s):invalid register details
public void testRegister010() {
	try{
		Register testTarget = new Register();
		testTarget.saveUser("1111111111111111111111","11","11","11111111111");
	}catch(Exception e){
		assertSame("invalid register details", e.getMessage());
	}
}

// Test Number: 011

		// Test Objective: Test register with invalid user_name,
	                 //valid password,invalid email,valid phoneNumber
		// Input(s): "1111111111111111111111","11","1111111111111111111111","11"
		// Expected Output(s):invalid register details
public void testRegister011() {
	try{
		Register testTarget = new Register();
		testTarget.saveUser("1111111111111111111111","11","1111111111111111111111","1111111111");
	}catch(Exception e){
		assertSame("invalid register details", e.getMessage());
	}
}
// Test Number: 012

			// Test Objective: Test register with invalid user_name,
		                 //valid password,invalid email,invalid phoneNumber
			// Input(s): "11","11","1111111111111111111111","11111111111"
			// Expected Output(s):invalid register details
public void testRegister012() {
	try{
		Register testTarget = new Register();
		testTarget.saveUser("1111111111111111111111","11","1111111111111111111111","11111111111");
	}catch(Exception e){
		assertSame("invalid register details", e.getMessage());
	}
}
// Test Number: 013

// Test Objective: Test register with invalid user_name,
             //invalid password,valid email,valid phoneNumber
// Input(s): "11","1111111111111111111111","11","11"
// Expected Output(s):invalid register details
public void testRegister013() {
	try{
		Register testTarget = new Register();
		testTarget.saveUser("1111111111111111111111","1111111111111111111111","11","1111111111");
	}catch(Exception e){
		assertSame("invalid register details", e.getMessage());
	}
}
// Test Number: 014

	// Test Objective: Test register with invalid user_name,
                 //invalid password,valid email,invalid phoneNumber
	// Input(s): "1111111111111111111111","1111111111111111111111","11","11111111111"
	// Expected Output(s):invalid register details
public void testRegister014() {
	try{
		Register testTarget = new Register();
		testTarget.saveUser("1111111111111111111111","1111111111111111111111","11","11111111111");
	}catch(Exception e){
		assertSame("invalid register details", e.getMessage());
	}
}
// Test Number: 015

		// Test Objective: Test register with invalid user_name,
	                 //invalid password,invalid email,valid phoneNumber
		// Input(s): "1111111111111111111111","1111111111111111111111","1111111111111111111111","1111111111"
		// Expected Output(s):invalid register details
public void testRegister015() {
	try{
		Register testTarget = new Register();
		testTarget.saveUser("1111111111111111111111","1111111111111111111111","1111111111111111111111","1111111111");
	}catch(Exception e){
		assertSame("invalid register details", e.getMessage());
	}
}
// Test Number: 016

			// Test Objective: Test register with invalid user_name,
		                 //invalid password,invalid email,invalid phoneNumber
			// Input(s): "1111111111111111111111","1111111111111111111111","1111111111111111111111","11111111111"
			// Expected Output(s):invalid register details
public void testRegister016() {
	try{
		Register testTarget = new Register();
		testTarget.saveUser("1111111111111111111111","1111111111111111111111","1111111111111111111111","11111111111");
	}catch(Exception e){
		assertSame("invalid register details", e.getMessage());
	}
}
	//
	
	// Test Number: 0017
	// Test Objective: Test Login with invalid information
	// Input(s): "user1","124"
	// Expected Output(s):invalid details
	public void testLogin001() throws Exception{
		try{
		UserLogin test=new UserLogin();
		test.userLogin("user1","124");
		}catch(Exception e) 
		{
			assertSame("invalid details", e.getMessage());		 	
		}
	}
	// Test Number: 0018
	// Test Objective: Test Login with valid information
	// Input(s): "user1","123"
	// Expected Output(s):user1 123
	public void testLogin002()throws Exception{
		String result= "user1 123";
		try{
		AdminLogin test = new AdminLogin();
		test.login("user1","123");
		assertEquals("user1 123", result);
		}catch(Exception e){
			fail("Should not get here...No exception expected"); 
		}
	}
	// Test Number: 0019
		// Test Objective: Test send the message to the valid address
		// Input(s): "1111","pujiayi2008@gmail.com"
		// Expected Output(s):Mail sent successfully
		public void testSendEmail001()throws Exception{
			SendGMail test = new SendGMail();
			String res = "Mail sent successfully";
			try 
			{	
		 	 	assertEquals(res, test.sendEmail("1111","pujiayi2008@gmail.com"));  	                                                    
			} 
			catch (Exception e) 
			{
				fail("Should not get here...No exception expected");      
			}
		}
		// Test Number: 0020
				// Test Objective: Test send the message to the invalid address
				// Input(s): "1111","pujiayi2008;gmail.com"
				// Expected Output(s):Mail sent successfully
				public void testSendEmail002()throws Exception{
					SendGMail test = new SendGMail();
					String res = "Malformed Email address";
					try 
					{	
						test.sendEmail("1111","pujiayi2008;gmail.com");  	                                                    
					} 
					catch (Exception e) 
					{
						assertSame("Malformed Email address",e.getMessage());      
					}
				}
				// Test Number: 0021
				// Test Objective: dont input the address
				// Input(s): "1111",""
				// Expected Output(s):exception
				public void testSendEmail003()throws Exception{
					SendGMail test = new SendGMail();
					try 
					{
						test.sendEmail("Test1","");  	     		
					} 
					catch (Exception e) 
					{
						assertSame("Malformed Email address",e.getMessage());  		
					}
				}
		
		// Test Number: 0022
				// Test Objective: Test getback the user password by email
				// Input(s): jiajia(the user store in the hsql)
				// Expected Output(s):successful
		public void testSGetbackpassword001()throws Exception{
			UserLogin test = new UserLogin();
			String res = "successful";
			try 
			{	
		 	 	assertEquals(res, test.getBackPassword("jiajia"));  	                                                    
			} 
			catch (Exception e) 
			{
				fail("Should not get here...No exception expected");      //indicator of unexpected failure case
			}
		}
		
}










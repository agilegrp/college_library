package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import controller.DeleteItem;

public class DeleteItemTest {

	// Test Number: 001
		// Test Objective: To test for ref 
		// Input(s): reference = 112222 
		// Expected Output(s) = invalid Entity
		@Test
		public void testgetref001() throws Exception {
			try{
			DeleteItem test = new DeleteItem();
			test.getref("1111111");
			}catch (Exception e) 
			{
				assertSame("Invaild Entity", e.getMessage());		 	
			}
		}
		// Test Number: 002
		// Test Objective: To test for Ref cancel
		// Input(s): Ref = 4
		// Expected Output(s) = Req  	
		@Test
		public void testgetref002() throws Exception {
			String result= "0";

		try{
			DeleteItem test = new DeleteItem();
			test.getref("0");
			assertEquals("0", result); 

		}catch (Exception e){
			fail("Should not get here...No exception expected"); 
			}
		
		}
}

package Doa;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LibraryDao 
{
	private static int flag = 0;
	public static Connection getConnection()
	{
		Connection connection = null;
		try 
		{
	 		Class.forName("org.hsqldb.jdbcDriver");
			connection = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/oneDB", "sa", "");
		} 
		catch (Exception e) 
		{
			System.out.println( "Get Connection " + e.getMessage());
		}
		return connection;
	}// end getConnection
	
	public Boolean chkLoginAdmin(String name, String password)
	{
		Connection connection = getConnection();
		ResultSet rs = null;
		Boolean res = false;
		String pPass = "";
		PreparedStatement psmt;
		try 
		{
			psmt = connection.prepareStatement("Select password from admin where admin_name = ? ");
			psmt.setString(1, name);
	 		rs = psmt.executeQuery();

			if(rs.next())
			{
				pPass =	rs.getString("password");	
			}
	 		psmt.close();
	  		connection.close();
		} 
		catch (Exception e) 
		{
			System.out.println( "Check Login " + e.getMessage());
		}
		if (password.equals(pPass)) res = true;
		System.out.println("logged in = " + res);
		return res;
		
	}//end chkLogin
	
	public void updateDB(String cmd) 
	{
		Connection connection = getConnection();
	  	ResultSet rs = null;
		String rate = "";
		PreparedStatement psmt;
		try 
		{
	 		psmt = connection.prepareStatement(cmd);
			psmt.executeUpdate();

			psmt.close();
			connection.close();
		}
	 	catch (Exception e) 
		{
			System.out.println( "updateDB " + e.getMessage());
		}	

	}//end updateDB
	
	public ResultSet queryDB(String cmd) throws Exception
	{
		Connection connection = getConnection();
	  	ResultSet rs = null;
		
		PreparedStatement psmt;

		try 
	 	{
			psmt = connection.prepareStatement(cmd);
	 		rs = psmt.executeQuery();
		
			psmt.close();
			connection.close();
		} 
	  	catch (SQLException e) 
	 	{
			e.printStackTrace();
		}
	
		return rs;
	}//end queryDB
	

}//end class

package controller;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import controller.FineAdmin;
import Doa.LibraryDao;

public class loanAdmin 
{
	LibraryDao lbD = new LibraryDao();
	FineAdmin fa = new FineAdmin();
	
	public Boolean insertNewLoan(String emailId, String bookTitle, int stPage,
			int stopPage) 
	{  
		
	   	ResultSet rs = null;
	 	String cmd = "";
	 	int usrId = -1;
	 	int itemId = -1;
	 	int numLoans = 0;
	 	int numStock = 0;
	 	
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	      	Date todaysDate = new Date();
	  	String today = df.format(todaysDate);
	  	String returnDate = addDate(14);
		Boolean res = false;
		try 
		{
			cmd = "select user_id from user where EMAIL = '" + emailId+ "'";
			rs = lbD.queryDB(cmd);
			
			while (rs.next())
			{
				usrId = rs.getInt("USER_ID");
			}
		 	if (usrId == -1) throw new Exception("User not found");
			
			cmd = "select * from item where title = '" + bookTitle+ "'";
			rs = lbD.queryDB(cmd);
			
			while (rs.next())
			{
	  			itemId = rs.getInt("ITEM_ID");
				numLoans = rs.getInt("NUMLOANS");
				numStock = rs.getInt("STOCKNUMBER");
				
			}
			if (itemId == -1) throw new Exception("Item not found");
			if (numStock > 0)
			{
				cmd = "insert into loan values (null,'"+usrId+"','"+today+"','"
			 				+returnDate+"','"+returnDate+"',0,'"+itemId+"')";
		 		lbD.updateDB(cmd);
		 		
				numLoans++;
				numStock--;
				
			  	cmd = "update item set NUMLOANS = "+numLoans+ "where item_id = "+itemId;
		 		lbD.updateDB(cmd);
				
				cmd = "update item set STOCKNUMBER = "+numStock+ "where item_id = "+itemId;
				lbD.updateDB(cmd);
				
				res = true;
	 		}
			else
			{
				throw new Exception("No copies left");
			}
	 		
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		return res;
		
	}
	
	public double loanReturn(String emailId, String bookTitle)
	{
	   	ResultSet rs = null;
	 	String cmd = "";
	 	int usrId = -1;
	 	int loanId = -1;
	 	int itemId = 0;
	 	int numLoans = 0;
	 	int numStock = 0;
	 	String loanDate = "";
	 	double fineAmount = 0.0;
	 	
	 	
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	      	Date todaysDate = new Date();
	  	String today = df.format(todaysDate);
	  	String returnDate = addDate(14);
		Boolean res = false;
		
		
		try 
		{
			cmd = "select user_id from user where EMAIL = '" + emailId+ "'";
			rs = lbD.queryDB(cmd);
			
			while (rs.next())
			{
				usrId = rs.getInt("USER_ID");
			}
		 	if (usrId == -1) throw new Exception("User not found");
			
			cmd = "select * from loan where user_id = '" + usrId+ "'";
			
			rs = lbD.queryDB(cmd);
			
			while (rs.next())
			{
	  			loanId = rs.getInt("LOAN_ID");
		 		loanDate = rs.getString("LOANDATE");
				itemId = rs.getInt("ITEM_ID");
				
			}

			long numDays = fa.calculateDays(loanDate);
			if (numDays > 14) 
			{
			 fineAmount = fa.calculateFine((int)numDays);
			}
			cmd = "update loan set DATERETURNED = '"+today+"'where user_id = "+usrId;
	 		lbD.updateDB(cmd);
	 		
			cmd = "update loan set LOANDATE = '"+today+"'where user_id = "+usrId;
	 		lbD.updateDB(cmd);
	 		
			cmd = "select * from item where item_id = '" + itemId+ "'";
			rs = lbD.queryDB(cmd);
			
			while (rs.next())
			{
	  			numLoans = rs.getInt("NUMLOANS");
				numStock = rs.getInt("STOCKNUMBER");	
			}
	 		
			numLoans--;
			numStock++;
			
		  	cmd = "update item set NUMLOANS = "+numLoans+ "where item_id = "+itemId;
	 		lbD.updateDB(cmd);
			
			cmd = "update item set STOCKNUMBER = "+numStock+ "where item_id = "+itemId;
			lbD.updateDB(cmd);
			
			res = true;
	 		
	 		
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		return fineAmount;
	}
	
	public String addDate(int num)
	{
		Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));
          cal.add(Calendar.DATE, + num);
        String newdate = cal.get(Calendar.DATE) +"/" +
                (cal.get(Calendar.MONTH)+1) + "/" + cal.get(Calendar.YEAR);
        
        String[] parts = newdate.split("/");
         int dy = Integer.parseInt(parts[0]);
         int mn = Integer.parseInt(parts[1]);
        if (dy < 10) parts[0] = "0"+parts[0];
        if (mn < 10) parts[1] = "0"+parts[1];
        return parts[2]+"-"+parts[1]+"-"+parts[0];  
        
	}//end method
	
}//end class

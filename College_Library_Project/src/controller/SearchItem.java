package controller;

import java.sql.ResultSet;
import model.Item;

import Doa.LibraryDao;

public class SearchItem {
	 LibraryDao lbD = new LibraryDao();
	 Item item = null;
	
	public Item searchBookTitle(String bookTitle) throws Exception 
	{ 
		Item item = null;
		ResultSet rs= null;	
		String resultBT = "";
		String cmd ="Select * from ITEM where TITLE = '"+bookTitle+"';";
    	try{		    	
	    	rs=lbD.queryDB(cmd);
				if (rs.next())
				{
					item = new Item(Integer.parseInt(rs.getString("ITEM_ID")),rs.getString("TITLE"),rs.getString("SUBJECT"),rs.getString("AUTHOR"),rs.getString("PUBLISHER"),rs.getString("GENRE"),Integer.parseInt(rs.getString("ISBN")),1,Integer.parseInt(rs.getString("STOCKNUMBER")));
					
					resultBT = rs.getString("TITLE");
					String resultBA = rs.getString("AUTHOR");
					String resultISBN = rs.getString("ISBN");
					String resultBid = rs.getString("ITEM_ID");
					String resultBStockNo = rs.getString("STOCKNUMBER");
					
					System.out.println("TITLE : " + resultBT);
					System.out.println("AUTHOR : " + resultBA);
					System.out.println("ISBN : " + resultISBN);
					System.out.println("ITEM_ID : " + resultBid);
					System.out.println("STOCKNUMBER : " + resultBStockNo);
					
				}
    	}catch (Exception e){
    		e.printStackTrace();
    		}
    	
    	
    	if (resultBT == ""){System.out.println(" This book is not in the library");}
		  		
	return item;
	
 }

	public Item searchJournalTitle(String journalTitle) throws Exception 
	{ 
		Item item = null;
		ResultSet rs= null;	
		String resultJT = "";
		String cmd ="Select * from ITEM where TITLE = '"+journalTitle+"';";
    	try{		    	
	    	rs=lbD.queryDB(cmd);
				if (rs.next())
				{
					item = new Item(Integer.parseInt(rs.getString("ITEM_ID")),rs.getString("TITLE"),rs.getString("SUBJECT"),rs.getString("AUTHOR"),rs.getString("PUBLISHER"),rs.getString("GENRE"),Integer.parseInt(rs.getString("ISBN")),1,Integer.parseInt(rs.getString("STOCKNUMBER")));
					
					resultJT = rs.getString("TITLE");
					String resultJA = rs.getString("AUTHOR");
					String resultJid = rs.getString("ITEM_ID");
					String resultJStockNo = rs.getString("STOCKNUMBER");
					
					System.out.println("TITLE : " + resultJT);
					System.out.println("AUTHOR : " + resultJA);
					System.out.println("ITEM_ID : " + resultJid);
					System.out.println("STOCKNUMBER : " + resultJStockNo);
					
				}
    	}catch (Exception e){
    		e.printStackTrace();
    		}
    	if (resultJT == ""){System.out.println(" This journal is not in the library");}
		  		
	return item;
	}
	
	
	public Item searchBookAuthor(String bookAuthor) throws Exception 
	{ 
		Item item = null; 
		ResultSet rs= null;	
		String resultBA = "";
		String cmd ="Select * from ITEM where AUTHOR = '"+bookAuthor+"';";
    	try{		    	
	    	rs=lbD.queryDB(cmd);
				if (rs.next())
				{
					item = new Item(Integer.parseInt(rs.getString("ITEM_ID")),rs.getString("TITLE"),rs.getString("SUBJECT"),rs.getString("AUTHOR"),rs.getString("PUBLISHER"),rs.getString("GENRE"),Integer.parseInt(rs.getString("ISBN")),1,Integer.parseInt(rs.getString("STOCKNUMBER")));
					
					resultBA = rs.getString("AUTHOR");
					String resultBT = rs.getString("TITLE");
					String resultISBN = rs.getString("ISBN");
					String resultBid = rs.getString("ITEM_ID");
					String resultBStock = rs.getString("STOCKNUMBER");
					
					System.out.println("TITLE : " + resultBT);
					System.out.println("AUTHOR : " + resultBA);
					System.out.println("ISBN : " + resultISBN);
					System.out.println("ITEM_ID : " + resultBid);
					System.out.println("STOCKNUMBER : " + resultBStock);
					
				}
    	}catch (Exception e){
    		e.printStackTrace();
    		}
    	if (resultBA == ""){System.out.println(" This book is not in the library");}
		  		
	return item;
	
 }
	public Item searchJournalAuthor(String journalAuthor) throws Exception 
	{ 
		Item item = null;
		ResultSet rs= null;	
		String resultJA = "";
		String cmd ="Select * from ITEM where AUTHOR = '"+journalAuthor+"';";
    	try{		    	
	    	rs=lbD.queryDB(cmd);
				if (rs.next())
				{
					item = new Item(Integer.parseInt(rs.getString("ITEM_ID")),rs.getString("TITLE"),rs.getString("SUBJECT"),rs.getString("AUTHOR"),rs.getString("PUBLISHER"),rs.getString("GENRE"),Integer.parseInt(rs.getString("ISBN")),1,Integer.parseInt(rs.getString("STOCKNUMBER")));
					
					resultJA = rs.getString("AUTHOR");
					String resultJT = rs.getString("TITLE");
					String resultJid = rs.getString("ITEM_ID");
					String resultJStockNo = rs.getString("STOCKNUMBER");
					
					System.out.println("TITLE : " + resultJT);
					System.out.println("AUTHOR : " + resultJA);
					System.out.println("ITEM_ID : " + resultJid);
					System.out.println("STOCKNUMBER : " + resultJStockNo);
					
					
				}
    	}catch (Exception e){
    		e.printStackTrace();
    		}
    	if (resultJA == ""){System.out.println(" This journal author is not in the library");}
		  		
	return item;
	
 }
	
	public Item searchBookRefNo(String bookID) throws Exception 
	{ 
		Item item = null;
		ResultSet rs= null;	
		String resultBid = "";
		String cmd ="Select * from ITEM where ITEM_ID = '"+bookID+"';";
    	try{		    	
	    	rs=lbD.queryDB(cmd);
				if (rs.next())
				{
					item = new Item(Integer.parseInt(rs.getString("ITEM_ID")),rs.getString("TITLE"),rs.getString("SUBJECT"),rs.getString("AUTHOR"),rs.getString("PUBLISHER"),rs.getString("GENRE"),Integer.parseInt(rs.getString("ISBN")),1,Integer.parseInt(rs.getString("STOCKNUMBER")));
										
					resultBid = rs.getString("ITEM_ID");
					String resultBA = rs.getString("AUTHOR");
					String resultBT = rs.getString("TITLE");
					String resultISBN = rs.getString("ISBN");
					String resultBStock = rs.getString("STOCKNUMBER");
					 
					System.out.println("TITLE : " + resultBT);
					System.out.println("AUTHOR : " + resultBA);
					System.out.println("ISBN : " + resultISBN);
					System.out.println("ITEM_ID : " + resultBid);
					System.out.println("STOCKNUMBER : " + resultBStock);
					
				}
    	}catch (Exception e){
    		e.printStackTrace();
    		}
    	if (resultBid == ""){System.out.println(" This book is not in the library");}
		  		
	return item;
 }
	
	public Item searchJournalRefNo(String journalID) throws Exception 
	{ 
		Item item = null;
		ResultSet rs= null;	
		String resultJid = "";
		String cmd ="Select * from ITEM where ITEM_ID = '"+journalID+"';";
    	try{		    	
	    	rs=lbD.queryDB(cmd);
				if (rs.next())
				{
					item = new Item(Integer.parseInt(rs.getString("ITEM_ID")),rs.getString("TITLE"),rs.getString("SUBJECT"),rs.getString("AUTHOR"),rs.getString("PUBLISHER"),rs.getString("GENRE"),Integer.parseInt(rs.getString("ISBN")),1,Integer.parseInt(rs.getString("STOCKNUMBER")));
										
					resultJid = rs.getString("ITEM_ID");
					String resultBA = rs.getString("AUTHOR");
					String resultBT = rs.getString("TITLE");
					String resultISBN = rs.getString("ISBN");
					String resultJStockNo = rs.getString("STOCKNUMBER");
					 
					System.out.println("TITLE : " + resultBT);
					System.out.println("AUTHOR : " + resultBA);
					System.out.println("ISBN : " + resultISBN);
					System.out.println("ITEM_ID : " + resultJid);
					System.out.println("STOCKNUMBER : " + resultJStockNo);
					
				}
    	}catch (Exception e){
    		e.printStackTrace();
    		}
    	if (resultJid == ""){System.out.println(" This book is not in the library");}
		  		
	return item;
 }
	
	public Item searchBookISBN(String bookISBN) throws Exception 
	{ 
		Item item = null;
		ResultSet rs= null;	
		String resultBISBN = "";
		String cmd ="Select * from ITEM where ISBN = '"+bookISBN+"';";
    	try{		    	
	    	rs=lbD.queryDB(cmd);
				if (rs.next())
				{
					item = new Item(Integer.parseInt(rs.getString("ITEM_ID")),rs.getString("TITLE"),rs.getString("SUBJECT"),rs.getString("AUTHOR"),rs.getString("PUBLISHER"),rs.getString("GENRE"),Integer.parseInt(rs.getString("ISBN")),1,Integer.parseInt(rs.getString("STOCKNUMBER")));
										
					resultBISBN = rs.getString("ISBN");
					String resultBA = rs.getString("AUTHOR");
					String resultBT = rs.getString("TITLE");
					String resultBid = rs.getString("ITEM_ID");
					String resultBStock = rs.getString("STOCKNUMBER");
					 
					System.out.println("TITLE : " + resultBT);
					System.out.println("AUTHOR : " + resultBA);
					System.out.println("ISBN : " + resultBISBN);
					System.out.println("ITEM_ID : " + resultBid);
					System.out.println("STOCKNUMBER : " + resultBStock);
					
				}
    	}catch (Exception e){
    		e.printStackTrace();
    		}
    	if (resultBISBN == ""){System.out.println(" This book is not in the library");}
		  		
	return item;
 }
	
		
}


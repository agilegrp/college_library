package controller;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Doa.LibraryDao;
import model.Item;

public class AddItem {
	public    boolean saveItem(int id,String title,String subject,String author,String publisher,String genre, int ISBN, int numloans, int stocknumber)throws Exception{
		
		Item item =new Item();
		item.setId(id);
		item.setTitle(title);
		item.setAuthor(author);
		item.setGenre(genre);
		
		item.setISBN(ISBN);
		item.setNumloans(numloans);
		item.setPublisher(publisher);
		item.setStocknumber(stocknumber);
		
		item.setSubject(subject);
		if(title==null||subject==null||author==null||genre==null||publisher==null){
			throw new Exception("invaild");
	}else{
		
		
		Connection conn= LibraryDao.getConnection();
		boolean res =false;
		String cmd= " insert into item(item_id,title,subject,author,publisher,genre,isbn,numloans,stocknumber) VALUES(?,?,?,?,?,?,?,?,?)";
		try {
			PreparedStatement psmt = conn.prepareStatement(cmd);
	
			psmt.setInt(1, id);
			psmt.setString(2, title);
			psmt.setString(3, subject);
			psmt.setString(4, author);
			psmt.setString(5, publisher);
			psmt.setString(6,genre);
			psmt.setInt(7, ISBN);
			psmt.setInt(8, numloans);
		
			
			psmt.setInt(9, stocknumber);
			
			psmt.executeUpdate();
			conn.close();
		
			res=true;
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		
		return res;
		
	}

}
  //}
}


package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import junit.framework.Test;
import model.User;
import Doa.LibraryDao;
import GUI.ViewGUI;

public class ViewReq
{ 
	//JTextField uname;
	LibraryDao lbDView = new LibraryDao();
	public User user;
	public String MostReqBook() 
	{
		ResultSet rs = null;
		String cmd = "SELECT TITLE  FROM ITEM WHERE LENGTH(isbn)  > 8  AND  NUMLOANS = (SELECT MAX(NUMLOANS) FROM ITEM)";
		String Btitle = "";

		
		try 
		{
			rs = lbDView.queryDB(cmd);
			while (rs.next()){
			Btitle = rs.getString("TITLE");
			
			}
		} 
	 	catch (Exception e) 
		{
	  		e.getMessage();
		} 
	
		if(Btitle==""){Btitle = "No Books yet Requested";}
		return Btitle; 
	}
	
	
	
	public String MostReqJ() 
	{
		ResultSet rs = null;
		String cmd = "SELECT TITLE  FROM ITEM WHERE LENGTH(isbn)  < 9  AND  NUMLOANS = (SELECT MAX(NUMLOANS) FROM ITEM)";
		String jtitle = "";
		
		try 
		{
			rs = lbDView.queryDB(cmd);
			while (rs.next()){
				jtitle = rs.getString("TITLE");
		
				}
			
			}
		
	 	catch (Exception e) 
		{
	  		e.getMessage();
		} 
		if(jtitle==""){jtitle = "No Journals yet Requested";}
		 return jtitle;
	}
	
	
	public String userFav(int id) 
	{
		String res = null;
	
		String fav0 = "";
		String fav1 ="";
		String fav2 ="";
		String fav3 ="";
		String fav4 ="";
		
		
		if (id == 0){
			res = "Your logged in as Admin";
			}else{
		
		ResultSet rs = null;
		String cmd = "SELECT * FROM FAVORITE WHERE USER_ID = '" + id+ "'";
		try 
		{
			rs = lbDView.queryDB(cmd);
			while (rs.next()){
				
				fav0 = rs.getString("FAV0");
				fav1 = rs.getString("FAV1");
				fav2 = rs.getString("FAV2");
				fav3 = rs.getString("FAV3");
				fav4 = rs.getString("FAV4");				
				}
			
				
			}
		
	 	catch (Exception e) 
		{
	  		e.getMessage();
	  		res = "Problem in favorites";
		} }
		
	 	if(fav0== ""){res = "No favorites yet";}else{
		 res = fav0+"\n"+fav1+"\n"+fav2+"\n"+fav3+"\n"+fav4;
	 	}
	return res;
	}
	
	



//----------------------------------------------------------
public String addfav(int usrId, String s1, String s2, String s3, String s4, String s5)
{
	String res = null;
	Connection conn= LibraryDao.getConnection();
	if (usrId == 0){
		res= "Your logged in as Admin";
		}
	
	
	else{
		ResultSet rs = null;
		String cmd = "SELECT * FROM FAVORITE WHERE USER_ID = '" + usrId+ "'";
		try 
		{
			rs = lbDView.queryDB(cmd);
			if (rs.next()){
				String cmd2 = "UPDATE FAVORITE SET  fav0 = ?,fav1 = ?, fav2 =? ,fav3 =?, fav4 =? WHERE USER_ID = " + usrId+ "";
				try  
				{
					PreparedStatement psmt = conn.prepareStatement(cmd2);
					psmt.setString(1, s1);
					psmt.setString(2, s2);
					psmt.setString(3, s3);
					psmt.setString(4, s4);
					psmt.setString(5,s5);
					psmt.executeUpdate();
					res = "table updated";
				}catch (Exception e) {
					e.getMessage();
					res ="Update error";
				} 
				}else{
						String cmd1 = "INSERT INTO FAVORITE (user_id,fav0,fav1,fav2,fav3,fav4) values (?, ?, ?, ?, ?, ?)";
						try 
						{
							PreparedStatement psmt1 = conn.prepareStatement(cmd1);
							psmt1.setInt(1, usrId);
							psmt1.setString(2, s1);
							psmt1.setString(3, s2);
							psmt1.setString(4, s3);
							psmt1.setString(5, s4);
							psmt1.setString(6,s5);
					
								psmt1.executeUpdate();
							res ="table created";
			}catch (Exception e) 
			{
		  		e.getMessage();
		  		res="Problem in update";
			} 
		
	}}catch (Exception e2) 
	{
  		e2.getMessage();
  		res="Error"; 
	} 

	}
	return res;
}}

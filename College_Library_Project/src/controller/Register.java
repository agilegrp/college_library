package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import Doa.LibraryDao;
import model.User;

public class Register {
	public boolean saveUser(String userName,String password,String email,String phoneNumber)throws Exception{
		User user= new User();
		user.setUserName(userName);
		user.setPassword(password);
		user.setEmail(email);
		user.setPhoneNumber(phoneNumber);
		if(userName.length()>20||password.length()>20||email.length()>40||phoneNumber.length()>10){
			throw new Exception("invalid register details");
		}else{
		Connection conn=LibraryDao.getConnection();
		boolean res=false;
		String cmd="insert into user(user_name,password,email,phoneNumber) values(?,?,?,?)";
		try {
			PreparedStatement ps = conn.prepareStatement(cmd);
			ps.setString(1,userName);
			ps.setString(2,password);
			ps.setString(3,email);
			ps.setString(4,phoneNumber);
			ps.executeUpdate();
			conn.close();
			res=true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}  
	}
}

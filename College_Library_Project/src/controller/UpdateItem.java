package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import Doa.LibraryDao;
import model.Item;

public class UpdateItem {
public  boolean updateItem(int ITEM_ID,String title,String subject,String author,String publisher,String genre, int ISBN, int numloans, int stocknumber){
		
		Item item =new Item();
		item.setId(ITEM_ID);
		item.setTitle(title);
		item.setAuthor(author);
		item.setGenre(genre);
		
		item.setISBN(ISBN);
		item.setNumloans(numloans);
		item.setPublisher(publisher);
		item.setStocknumber(stocknumber);
		
		item.setSubject(subject);
		
		
		
		Connection conn= LibraryDao.getConnection();
		boolean res =false;
		
		try{
		String cmds = "UPDATE ITEM SET " + "Title = '" + title + "', Genre = '"
				+ genre + "', Subject = '" +subject + "', Author ='" + author
				+ "', Publisher = '" + publisher + "', ISBN = '" + ISBN + "', Stocknumber = '" + stocknumber + "', Numloans = '"
				+ numloans + "' where ITEM_ID = " + ITEM_ID;

		
	
		
			PreparedStatement psmt = conn.prepareStatement(cmds);
	
			
			
			psmt.executeUpdate();
			conn.close();
		
		
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		res=true;
		return res;
		
}
		
		
		

}

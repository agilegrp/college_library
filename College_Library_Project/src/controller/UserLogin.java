package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import Doa.LibraryDao;
import model.User;

public class UserLogin {
	LibraryDao lbD = new LibraryDao();
	SendGMail sgm = new SendGMail();
	public User userLogin(String username,String password) throws Exception{
		User user = null;
		Connection conn=LibraryDao.getConnection();
		String cmd="select * from user where user_name =? and password=?";
		try{

			PreparedStatement ps = conn.prepareStatement(cmd);
			ps.setString(1,username);
			ps.setString(2,password);
			ResultSet rs=ps.executeQuery();
			if(rs.next()){
				user=new User();
				user.setId(rs.getInt("user_id"));
				user.setUserName(rs.getString("user_name"));
				user.setPassword(rs.getString("password"));
				user.setPhoneNumber(rs.getString("phoneNumber"));
				user.setEmail(rs.getString("email"));
			}else{
				throw new Exception("invalid details");
			}
			rs.close();
			ps.close();
			conn.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	public String 	getBackPassword(String username) throws Exception
	{
		ResultSet rs = null;
		String email="";
		String password="";
		String result = "error";
		String cmd = "select * from user where user_name = '"+username+"';";
		rs = lbD.queryDB(cmd);
		while (rs.next()){
		email= rs.getString("email");
		password=rs.getString("password");
		String ms="Your password is"+password;
		try {
			sgm.sendEmail(ms, email);
			result="successful";
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
		System.out.println(result);
		return result; 
	}


}

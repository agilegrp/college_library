package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;

import junit.framework.Test;
import Doa.LibraryDao;
import controller.SendGMail;

public class FineAdmin 
{
	LibraryDao lbD = new LibraryDao();
	SendGMail sgm = new SendGMail();
	public long calculateDays(String dateStart) 
	{
		long diffDays = 0;
		// HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	  	Date todaysDate = new Date();
	 	String dateToday = df.format(todaysDate);

		Date d1 = null;
		Date d2 = null;

		try 
		{
			d1 = df.parse(dateStart);
			d2 = df.parse(dateToday);

			// in milliseconds
			long diff = d2.getTime() - d1.getTime();
			diffDays = diff / (24 * 60 * 60 * 1000);
		} 
		catch (Exception e) 
		{
			e.getMessage();
		}
		return diffDays;
	}

	public double calculateFine(int numDays) throws Exception
	{
		double fine = 0; 
		Double fineRate = Double.parseDouble(viewFineRate());
		try 
		{
			if ((numDays == 0) || (fineRate == 0))
				throw new Exception("A zero value was entered");
			else
				fine = numDays * fineRate;
		 		fine = Math.round(fine*100.0)/100.0;
		} 
		catch (Exception e) 
		{
			e.getMessage();
		}
		return fine;
	}

	public Boolean updateFineRate(String name, String password, double fineRate) 
	{  
		
		Double rate = 0.0;
		
		Boolean res = false;
		try 
		{
			if (fineRate == 0) {
				throw new Exception("A zero value was entered"); 
			} 
			else 
			{
				Boolean idOk = lbD.chkLoginAdmin(name, password);
				if (idOk) 
				{	
					String cmd = "update admin set fineRate = '" + fineRate+ "'";
		   			lbD.updateDB(cmd);
		 			
		 			rate = Double.parseDouble(viewFineRate());
		 			if (rate.equals(fineRate))res = true;
				}
				else
				{
					throw new Exception("Incorrect password entered");
				}
			}
		} 
		catch (Exception e) 
		{
		 	e.getMessage();
		}
		return res;
	}

	public String viewFineRate() throws Exception
	{
		ResultSet rs = null;
		String cmd = "select fineRate from admin";
		
		String rate = "";

			rs = lbD.queryDB(cmd);
			
			while (rs.next())
			{
				rate = rs.getString("fineRate");
			}
 
		return rate; 
	}
	
	public String setUserFine(double fine, String usr, String emailId)
	{
		String cmd = "update loan set FINE = '" + fine+ "'"
				+ " where user_id = "+usr;
			lbD.updateDB(cmd);
			
			try {
				sgm.sendEmail("You have an over due fine", emailId);
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return "Fine updated";
	}
	
	public String checkFines() throws Exception
	{
	 	ResultSet rs = null;
	 	String result = "No Fines Pending";
		String cmd = "select * from loan";
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String usr;
		int numDays = 0;
		double fine = 0.0;

		rs = lbD.queryDB(cmd);
		
		while (rs.next())
		{
			String users = "";
			String userName = "";
	 		Date ld = rs.getDate("LoanDate");
			usr = rs.getString("USER_ID");
			
			String loanDate = df.format(ld);
			numDays = (int)calculateDays(loanDate);
			if (numDays > 14) 
			{
				cmd = "select * from user where user_id = "+usr;
				rs = lbD.queryDB(cmd);
				
				while (rs.next())
				{
					userName= rs.getString("EMAIL");
				}
				users = userName + "\n";
				fine = calculateFine(numDays);
				setUserFine(fine, usr, userName);
				
				result = "Fines Pending\n"+users;
				
			}
		}
		System.out.println(result);
		return result; 
	}
	
	public String checkMyLoans(int usr) throws Exception
	{
	 	ResultSet rs = null;
	 	String result = "No Fines Pending";
		String cmd = "select * from loan where user_id = "+usr;
		
		rs = lbD.queryDB(cmd);
		
		while (rs.next())
		{
			double fine = rs.getDouble("FINE");
			
			if (fine > 0) 
			{
				result = "Fines pending";
			}
		}
		return result; 
	}

}

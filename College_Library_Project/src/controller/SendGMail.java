package controller;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendGMail 
{

	public  String sendEmail(String mesg, String recvrMail) throws Exception
	{
	 	String result = "";
	 	Boolean emailChk = false;
		
		char[] charArray = recvrMail.toCharArray();
		for (int i = 0; i < charArray.length; i++)
		{
			if (charArray[i] == '@' ) emailChk = true;
		}
		if (emailChk == false) throw new Exception("Malformed Email address");

		// Librarian email and password
		final String username = "collipproj@gmail.com";
		final String pass = "AgileGrp3!";
		
		Properties props = new Properties();
	    props.put("mail.smtp.host", "smtp.gmail.com");
	    props.put("mail.smtp.socketFactory.port", "465");
	    props.put("mail.smtp.socketFactory.class",
	                "javax.net.ssl.SSLSocketFactory");
	    props.put("mail.smtp.auth", "true");
	    props.put("mail.smtp.port", "465");
			//props.setProperty("mail.smtp.host", "localhost");
//			props.put("mail.smtp.starttls.enable","true");
//			props.put("mail.smtp.auth", "true");
//			props.put("mail.smtp.startls.enable", "true");
//			props.put("mail.smtp.host", "smtp.gmail.com");
//			props.put("mail.smtp.port", "587"); 
		 
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator()
				{
				protected PasswordAuthentication getPasswordAuthentication()
				{
					return new PasswordAuthentication(username, pass);
				}
		});
		
		try 
		{
		    // Create a message and send it as an email
		    Message mailMsg = new MimeMessage(session);

		    mailMsg.setFrom(new InternetAddress("collipproj@gmail.com"));
	 	    mailMsg.setRecipients(Message.RecipientType.TO, 
		    		InternetAddress.parse(recvrMail));
		 
	 	    mailMsg.setSubject("Email from College Library");
		    mailMsg.setSentDate(new Date());
		    mailMsg.setText(mesg);
		    try {
				Transport.send(mailMsg);
				result = "Mail sent successfully";
			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}

		catch (Exception e) 
		{
		     result =e.getMessage();
		}
		return result;
	}
}

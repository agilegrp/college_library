package model;

public class Item {
	private int id;
	private String title;
	private String subject;
	private String author;
	
	private String publisher;
	private String genre;
	private int ISBN;
	private int numloans;
	
	private int stocknumber;
	
	
	public 	Item(){
		
	}
	public Item(int id, String title, String subject,String author, String publisher,String genre,int ISBN,int numloans,int stocknumber)
	{
		this.id=id;
		this.title=title;
		this.subject=subject;
		this.author=author;
		this.publisher=publisher;
		this.genre=genre;
		this.ISBN=ISBN;
		this.numloans=numloans;
		
		this.stocknumber=stocknumber;
		
	}
	
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id=id;
		
	}
	
	public String getTitle(){
		return title;
		
	}
	
	public void setTitle(String title){
		this.title=title;
	}
	
	public String getAuthor(){
		return author;
	}
	
	public void setAuthor(String author){
		this.author=author;
		
	}
	
	public int getISBN(){
		return ISBN;
		
	}
	
	public void setISBN(int ISBN){
		this.ISBN=ISBN;
		
	}
	
	
	public int getNumloans(){
		return numloans;
	}
	 
	public void setNumloans(int numloans)
	{
		this.numloans=numloans;
		
	}
	
	public String getSubject(){
		return subject;
		
	}
	
	public void setSubject(String subject){
		this.subject=subject;
		
	}
	
	public String getGenre(){
		return genre;
	}
	
  public void setGenre(String genre){
	  this.genre=genre;
  }
  
  
  public int getStocknumber(){
	  
  
  return stocknumber;
}
  public void setStocknumber(int stocknumber){
	  this.stocknumber=stocknumber;
  }
  
	public String getPublisher(){
		return publisher;
	}
	
  public void setPublisher(String publisher){
	  this.publisher=publisher;
  }
}
